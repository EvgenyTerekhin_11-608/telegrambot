using Decide.CodeGenerator;
using Decide.CodeGenerator.Generator;
using Decide.SpreadSheetProvider;
using Decide.TelegramBot;
using Force;
using Microsoft.CodeAnalysis;
using NUnit.Framework;

namespace Tests
{
    public class UseMeGenerationTest
    {
        private VisitorInfoFactory VisitorInfoFactory { get; set; }
        private UploadModelInSpreadSheet uploadApi { get; set; }
        private GoogleSheetApi api { get; set; }
        private StructureCreatorWithReplace StructureCreatorWithReplace { get; set; }
        private CreateClassHandler CreateClassHandler { get; set; }

        private IHandler<VisitorInfo, string> ReWriter { get; set; }
        private IHandler<WriteFileInfo> ClassToFIleWriter { get; set; }
        private IHandler<EnumValuesInfo, SyntaxTree> enumGenerator { get; set; }

        [SetUp]
        public void Setup()
        {
            api = new GoogleSheetApi();
            VisitorInfoFactory = new VisitorInfoFactory();
            uploadApi = new UploadModelInSpreadSheet(api);
            var replaceVisitor = new ReplaceVisitor();
            ReWriter = new ReplaceHandler(replaceVisitor);
            ClassToFIleWriter = new TextWriterHandler();
            CreateClassHandler = new CreateClassHandler(ReWriter, ClassToFIleWriter);
            enumGenerator = new EnumGenerator();
            StructureCreatorWithReplace =
                new StructureCreatorWithReplace(CreateClassHandler, enumGenerator, uploadApi, VisitorInfoFactory);
        }

        [Test]
        public void Test()
        {
            StructureCreatorWithReplace.Create();
        }
    }
}