using System;
using System.Threading;
using Decide.EventsAnalyzer;
using DIContainer;
using Force;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NotifyModule;
using NUnit.Framework;
using TestFSharp;
using TestFSharp.Domain;
using TestFSharp.Handlers;

namespace Tests
{
    public class TimeNotifeTests
    {
        private TimeSpan Second_1 = new TimeSpan(0, 0, 0, 1);
        private TimeSpan Second_5 = new TimeSpan(0, 0, 0, 5);
        private TimeSpan Second_10 = new TimeSpan(0, 0, 0, 10);
        private TimeSpan Second_20 = new TimeSpan(0, 0, 0, 20);

      

        [SetUp]
        public void Setup()
        {
            var context = new BotDbContext();

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            IOC.Container.Register<INotificationHandler<Test>, TestHandler>();
            IOC.Container.Register<DbContext, BotDbContext>(context);

            IOC.Container.Register<IHandler<Func<Type, bool>, Type>, GetTypeHandler>();
            IOC.Container
                .Register<IHandler<string, GenericNotificationType>, GetNotifyGenericTypeHandler>();

            IOC.Container.Register<IHandler<GetNotificationHandlerInput>, DoNotificationHandler>();
            IOC.Container.Register<IHandler<AggregateNotificationDto>, NotificateHandler>();
            IOC.Container.Register<IHandler<Notification, Event>, CreateNotifyHandler>();
            IOC.Container.Register<IHandler<Event>, AddNotifyHandler>();
        }


        [Test]
        public void Registered__RegisteredDbContext__ReturnObject()
        {
            var aggregateDto = new AggregateNotificationDto("Test", JsonConvert.SerializeObject(new Test()));
            var notificateHandler = new NotificateHandler(new DoNotificationHandler(),
                new GetNotifyGenericTypeHandler(new GetTypeHandler()));
            notificateHandler.Handle(aggregateDto);
        }

        [Test]
        public void AddNotification__AddNorificationAndRegisterInEventsAnalyzer__DoEventAction()
        {
            var context = IOC.Container.Get<DbContext, BotDbContext>();
            var pupil = new Pupil(1, new EducationCard());
            var notification = new Notification(new NotificationHandlerInfo(new Test()),
                DateTime.Now.Add(Second_5).Add(Second_5),"Test");
            pupil.AddNotification(notification, context);
            var addNotifyHanlder = IOC.Container.Get<IHandler<Event>>();
            var createNotifyHandler = IOC.Container.Get<IHandler<Notification, Event>>();
            var @event = createNotifyHandler.Handle(notification);
            addNotifyHanlder.Handle(@event);

            Thread.Sleep(10000);
        }
    }

    public class Test : INotificationEntity
    {
        public string Name = "name";
    }

    public class TestHandler : INotificationHandler<Test>
    {
        public void Handle(Test test)
        {
            Console.WriteLine(test.Name);
        }
    }
}