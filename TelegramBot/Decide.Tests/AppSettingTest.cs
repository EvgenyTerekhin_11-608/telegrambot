using Decide.AppSettings;
using Decide.SpreadSheetProvider;
using NUnit.Framework;
using TestFSharp.Infrastructure;

namespace Tests
{
    public class AppSettingTest
    {
        [Test]
        public void TestSpreadSheetProvider()
        {
            var result = AppSettingsProvider.GetSetting<SpreadSheetProviderSetting>("Decide.SpreadSheetProvider");
            Assert.AreEqual(result.credentialPath, "credentials.json");
        }

        [Test]
        public void TestDomain()
        {
            var result = AppSettingsProvider.GetSetting<DomainSettings>("Decide.Domain");
            Assert.AreEqual(result.ConnectionString,
                "Username=postgres;Password=postgres;Host=localhost;Port=5432;Database=EducationBot");
        }
    }
}