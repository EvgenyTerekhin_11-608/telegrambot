using System;
using NotifyModule.Atttibutes;
using NUnit.Framework;
using TestFSharp;

namespace Tests
{
    public class TypeAttributeTest
    {
        [TestCase("1")]
        [TestCase("2")]
        [TestCase("150")]
        [TestCase("0163512")]
        public void Convert__AddEnumStringTypeWithStringValue__GetIntValue(string number)
        {
            var s = StringType.Int;
            var typeAttibute = StringType.Int.GetAttributeOfType<TypeConverterAttribute>();
            Assert.AreEqual(int.Parse(number),typeAttibute.Convert(number));
        }

        [Test]
        public void Convert__AddEnumStringTypeWithDateTimeValue__GetDateTimeValue()
        {
            var dateTime = DateTime.Now.ToString();
            var typeAttibute = StringType.DateTime.GetAttributeOfType<TypeConverterAttribute>();
            Assert.AreEqual(DateTime.Parse(dateTime), typeAttibute.Convert(dateTime));
        }
        
        [TestCase("1,1")]
        [TestCase("2,213")]
        [TestCase("150,214")]
        [TestCase("0163512,21424211")]
        public void Convert__AddEnumStringTypeWithDoubleValue__GetDateTimeValue(string _double)
        {
            var typeAttibute = StringType.Double.GetAttributeOfType<TypeConverterAttribute>();
            Assert.AreEqual(double.Parse(_double), typeAttibute.Convert(_double));
        }
        
        [TestCase("str")]
        [TestCase("avsdmspdfbfbi4jbi324jbg423i34")]
        [TestCase("0163512,21424211")]
        public void Convert__AddEnumStringTypeWithStringValue__GetDateTimeValue(string _string)
        {
            var typeAttibute = StringType.String.GetAttributeOfType<TypeConverterAttribute>();
            Assert.AreEqual(_string, typeAttibute.Convert(_string));
        }
        
        
    }
}