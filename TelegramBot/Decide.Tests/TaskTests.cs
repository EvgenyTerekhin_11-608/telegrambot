using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Tests
{
    public class TaskTests
    {
        private void Print(int s)
        {
            Console.WriteLine(s + " " + Thread.CurrentThread.ManagedThreadId);
        }

        [Test]
        public void TaskRunTest()
        {
            Print(1);
            Task.Run(delay);
            Print(2);
            Thread.Sleep(3000);
            Print(3);
        }

        [Test]
        public async Task TaskRunTest__1()
        {
            Print(1);
            await delay();
            Print(2);
            Thread.Sleep(1001);
            Print(3);
        }
       
        public async Task delay()
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            await Task.Delay(5000);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
        }
        
        [Test]
        public void Test123()
        {
            var token1 = new CancellationTokenSource();
            var token2 = new CancellationTokenSource();
            var token3 = new CancellationTokenSource();
            token1.Cancel();
            token3.Cancel();
            StartsTask(token1.Token, token2.Token, token3.Token);
            Thread.Sleep(3000);
        }

        public void StartsTask(CancellationToken token1, CancellationToken token2, CancellationToken token3)
        {
            Task.Delay(1000, token1).ContinueWith(x => { Console.WriteLine(1); }, token1);
            Task.Delay(1000, token2).ContinueWith(x => { Console.WriteLine(2); }, token2);
            Task.Delay(1000, token3).ContinueWith(x => { Console.WriteLine(3); }, token3);
        }

        public class Cancellation
        {
            public CancellationTokenSource TokenSource { get; set; }
        }

        [Test]
        public void TupleRefTest()
        {
            var abc = new Cancellation() {TokenSource = new CancellationTokenSource()};
            Do("One", abc);
            abc.TokenSource.Cancel();

            Thread.Sleep(3000);
        }

        public async void Do(string print, Cancellation cancellation)
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            await Task.Delay(1000)
                .ContinueWith(x => { Console.WriteLine(Thread.CurrentThread.ManagedThreadId); })
                .ContinueWith(x => { Console.WriteLine(Thread.CurrentThread.ManagedThreadId); })
                .ContinueWith(x => { Console.WriteLine(Thread.CurrentThread.ManagedThreadId); })
                .ContinueWith(x => { Console.WriteLine(Thread.CurrentThread.ManagedThreadId); });
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
        }
    }
}