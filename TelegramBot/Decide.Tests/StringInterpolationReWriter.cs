using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using NUnit.Framework;
using TestFSharp;
using TestFSharp.Domain;

namespace Tests
{
    public static class InterpolationReplace
    {
        public static IQueryable<T> Replace<T>(this IQueryable<T> qu)
        {
            var result = new InterpolationStringReplacer<T>().Visit(qu.Expression);
            var s = (IQueryable<T>) qu.Provider.CreateQuery(result);
            return s;
        }
    }

    public class InterpolationStringReplacer<T> : ExpressionVisitor
    {
        private readonly List<PatternMachingStructure> patternMatchingList;

        public InterpolationStringReplacer()
        {
            patternMatchingList = new List<PatternMachingStructure>
            {
                new PatternMachingStructure
                {
                    FilterPredicate = x => FormatMethodsWithObjects.Contains(x),
                    SelectorArgumentsPredicate = x => x.Arguments.Skip(1),
                    ReturnPredicate = InterpolationToStringConcat
                },
                new PatternMachingStructure
                {
                    FilterPredicate = x => FormatMethodWithArrayParameter.Contains(x),
                    SelectorArgumentsPredicate = x => ((NewArrayExpression) x.Arguments.Last()).Expressions,
                    ReturnPredicate = InterpolationToStringConcat
                },
                new PatternMachingStructure()
                {
                    FilterPredicate = x => true,
                    SelectorArgumentsPredicate = x => x.Arguments,
                    ReturnPredicate = (node, _) => base.VisitMethodCall(node)
                }
            };
        }

        private IEnumerable<MethodInfo> FormatMethods =>
            typeof(string).GetMethods().Where(x => x.Name.Contains("Format"));

        private IEnumerable<MethodInfo> FormatMethodsWithObjects => FormatMethods
            .Where(x =>
                x.GetParameters().All(xx =>
                    xx.ParameterType == typeof(string) || xx.ParameterType == typeof(object)));

        private IEnumerable<MemberInfo> FormatMethodWithArrayParameter => FormatMethods
            .Where(x => x.GetParameters().Any(xx => xx.ParameterType == typeof(object[])));

        private MethodInfo StringConcatMethod =>
            typeof(string).GetMethod("Concat", new Type[] {typeof(object), typeof(object)});


        private Expression InterpolationToStringConcat(MethodCallExpression node,
            IEnumerable<Expression> formatArguments)
        {
            //выбираем первый аргумент
            //(example : Format("Name: {0} Age: {1}", x.Name,x.Age) -> "Name: {0} Age: {1}"
            var formatString = node.Arguments.First();
            // проходимся по паттерну из метода Format и выбираем все строки между аргументами
            // передаем их методу ExpressionConstant
            // example : -> [Expression.Constant("Name: "),Expression.Constant(" Age: ")]
            var betweenArgumentStrings = Regex.Split(formatString.ToString(), 
                @"\{\d\}").Skip(1).Select(Expression.Constant);
            // мерджим их со значениями, пришедшими formatArguments
            var merge = formatArguments.Merge(betweenArgumentStrings);
            // склеиваем так, как QueryableProvider склеивает простую конкатенацию строк
            //(
            var s = merge.Aggregate((acc, cur) => Expression.Add(acc, cur, StringConcatMethod));
            return s;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var pattern = patternMatchingList.First(x => x.FilterPredicate(node.Method));
            var arguments = pattern.SelectorArgumentsPredicate(node);
            var expression = pattern.ReturnPredicate(node, arguments);
            return expression;
        }
    }

    public class PatternMachingStructure
    {
        public Func<MethodInfo, bool> FilterPredicate { get; set; }
        public Func<MethodCallExpression, IEnumerable<Expression>> SelectorArgumentsPredicate { get; set; }
        public Func<MethodCallExpression, IEnumerable<Expression>, Expression> ReturnPredicate { get; set; }
    }

    public static class EnumerableExtentions
    {
        public static IEnumerable<T> Merge<T>(this IEnumerable<T> e1, IEnumerable<T> e2)
        {
            var zip = e1.Zip(e2, (x, y) => new {x, y});
            foreach (var z in zip)
            {
                yield return z.x;
                yield return z.y;
            }
        }
    }


    public class ReplaceString
    {
        [Test]
        public void S()
        {
            var regex = Regex.Split("{0} {1}   {2}  {3}", @"\{\d\}").Skip(1);

            var context = new BotDbContext();
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            var id = new Random().Next(1000, 100000000);
            context.Add(new Pupil(id, new EducationCard()));
            context.SaveChanges();
            
            var pupil1 = new BotDbContext().Pupils.Where(x => x.ChatId == id)
                .Select(x => x.ChatId + "        " + x.ChatId)
                .OrderBy(x => x)
                .Expression.ToString();
            var pupil = new BotDbContext().Pupils.Where(x => x.ChatId == id)
                    .Select(x => $"Name:{x.ChatId}    -    Age:{x.ChatId}")
                    .Replace()
                    .OrderBy(x => x)
                ;


            var sss = pupil.First();


            var concatMethod = typeof(string).GetMethod("Concat", new[] {typeof(string), typeof(string)});
            var str_1 = Expression.Constant("a");
            var str_2 = Expression.Constant("b");
            var concat = Expression.Add(str_1, str_2, concatMethod);
            var ssss = pupil.First();
        }
    }
}