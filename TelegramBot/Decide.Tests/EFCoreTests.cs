using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Domain;
using Microsoft.EntityFrameworkCore.Internal;
using NUnit.Framework;
using TestFSharp;
using TestFSharp.Domain;

namespace Tests
{
    public class Visitor : ExpressionVisitor
    {
        public override Expression Visit(Expression node)
        {
            Console.WriteLine("Visit " + node);
            return base.Visit(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            Console.WriteLine("VisitBinary " + node);
            return base.VisitBinary(node);
        }

        protected override Expression VisitBlock(BlockExpression node)
        {
            Console.WriteLine("VisitBlock " + node);
            return base.VisitBlock(node);
        }

        protected override CatchBlock VisitCatchBlock(CatchBlock node)
        {
            Console.WriteLine("VisitCatchBlock " + node);
            return base.VisitCatchBlock(node);
        }

        protected override Expression VisitConditional(ConditionalExpression node)
        {
            Console.WriteLine("VisitConditional " + node);
            return base.VisitConditional(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            Console.WriteLine("VisitConstant " + node.Value);
            return base.VisitConstant(node);
        }

        protected override Expression VisitDebugInfo(DebugInfoExpression node)
        {
            Console.WriteLine("VisitDebugInfo " + node);
            return base.VisitDebugInfo(node);
        }

        protected override Expression VisitDefault(DefaultExpression node)
        {
            Console.WriteLine("VisitDefault " + node);
            return base.VisitDefault(node);
        }

        protected override Expression VisitDynamic(DynamicExpression node)
        {
            Console.WriteLine("VisitDynamic " + node);
            return base.VisitDynamic(node);
        }

        protected override ElementInit VisitElementInit(ElementInit node)
        {
            Console.WriteLine("VisitElementInit " + node);
            return base.VisitElementInit(node);
        }

        protected override Expression VisitExtension(Expression node)
        {
            Console.WriteLine("VisitExtension " + node);
            return base.VisitExtension(node);
        }

        protected override Expression VisitGoto(GotoExpression node)
        {
            Console.WriteLine("VisitGoto " + node);
            return base.VisitGoto(node);
        }

        protected override Expression VisitIndex(IndexExpression node)
        {
            Console.WriteLine("VisitIndex " + node);
            return base.VisitIndex(node);
        }

        protected override Expression VisitInvocation(InvocationExpression node)
        {
            Console.WriteLine("VisitInvocation " + node);
            return base.VisitInvocation(node);
        }

        protected override Expression VisitLabel(LabelExpression node)
        {
            Console.WriteLine("VisitLabel " + node);
            return base.VisitLabel(node);
        }

        protected override LabelTarget VisitLabelTarget(LabelTarget node)
        {
            Console.WriteLine("VisitLabelTarget " + node);
            return base.VisitLabelTarget(node);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            Console.WriteLine("VisitLambda " + node);
            return base.VisitLambda(node);
        }

        protected override Expression VisitListInit(ListInitExpression node)
        {
            Console.WriteLine("VisitListInit " + node);
            return base.VisitListInit(node);
        }

        protected override Expression VisitLoop(LoopExpression node)
        {
            Console.WriteLine("VisitLoop " + node);
            return base.VisitLoop(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            Console.WriteLine("VisitMember " + node);
            return base.VisitMember(node);
        }

        protected override MemberAssignment VisitMemberAssignment(MemberAssignment node)
        {
            Console.WriteLine("VisitMemberAssignment " + node);
            return base.VisitMemberAssignment(node);
        }

        protected override MemberBinding VisitMemberBinding(MemberBinding node)
        {
            Console.WriteLine("VisitMemberBinding " + node);
            return base.VisitMemberBinding(node);
        }

        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            Console.WriteLine("VisitMemberInit " + node);
            return base.VisitMemberInit(node);
        }

        protected override MemberListBinding VisitMemberListBinding(MemberListBinding node)
        {
            Console.WriteLine("VisitMemberListBinding  " + node);
            return base.VisitMemberListBinding(node);
        }

        protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding node)
        {
            Console.WriteLine("VisitMemberMemberBinding " + node);
            return base.VisitMemberMemberBinding(node);
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            Console.WriteLine("VisitMethodCall " + node);
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitNew(NewExpression node)
        {
            Console.WriteLine("VisitNew " + node);
            return base.VisitNew(node);
        }

        protected override Expression VisitNewArray(NewArrayExpression node)
        {
            Console.WriteLine("VisitNewArray " + node);
            return base.VisitNewArray(node);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            Console.WriteLine("VisitParameter " + node);
            return base.VisitParameter(node);
        }

        protected override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
        {
            Console.WriteLine("VisitRuntimeVariables " + node);
            return base.VisitRuntimeVariables(node);
        }

        protected override Expression VisitSwitch(SwitchExpression node)
        {
            Console.WriteLine("VisitSwitch " + node);
            return base.VisitSwitch(node);
        }

        protected override SwitchCase VisitSwitchCase(SwitchCase node)
        {
            Console.WriteLine("VisitSwitchCase " + node);
            return base.VisitSwitchCase(node);
        }

        protected override Expression VisitTry(TryExpression node)
        {
            Console.WriteLine("VisitTry " + node);
            return base.VisitTry(node);
        }

        protected override Expression VisitTypeBinary(TypeBinaryExpression node)
        {
            Console.WriteLine("VisitTypeBinary " + node);
            return base.VisitTypeBinary(node);
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            Console.WriteLine("VisitUnary " + node);
            return base.VisitUnary(node);
        }
    }

    public class EFCoreTests
    {
        private List<Human> _pupils { get; set; }
        private BotDbContext _context { get; set; }

        [SetUp]
        public void Setup1()
        {
            _context = new BotDbContext();
        }

//        [SetUp]
        public void Setup()
        {
            _context = new BotDbContext();
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            var ex1 = _context.Humans
                .Where(x => x.Age < 20)
                .OrderBy(x => x.Name)
                .Select(x => x.Name + " " + x.Surname)
                .Where(x => x.Length < 20);

            new Visitor().Visit(ex1.Expression);

            var ex2 = _context.Humans
                .ToList()
                .Where(x => x.Age < 20)
                .OrderBy(x => x.Name)
                .Select(x => x.Name + " " + x.Surname)
                .Where(x => x.Length < 20);


//            _context.Database.EnsureDeleted();
//            _context.Database.EnsureCreated();
            _pupils = new List<Human>()
            {
                new Human("Jonh", 12, "cool men"),
                new Human("Bob", 13, "cool men"),
                new Human("Mikky", 1488, "cool men"),
            };
            _context.AddRange(_pupils);
            _context.SaveChanges();
        }

        [Test]
        public void CorrectTest()
        {
//            Expression<Func<List<Human>, string>> xss = s => s.Select(x => $"{x.Id}").Aggregate((acc,cur)=>acc+cur);


            var result = _context.Humans.Select(x => new
            {
                age = $"{x.Age}",
                age1 = x.Age.ToString().Length
            }).Select(x => x.age);
        }

        [Test]
        public void
            Interpolation__CallInterpolationStringAndOrderByWithAppealInterpolationString__InvalidOperationException()
        {
            var result = _context.Humans.Select(x => $"{x.Age}").OrderBy(x => x);
            Assert.Throws<InvalidOperationException>(() => result.ToList());
        }

        [Test]
        public void
            Interpolation__CallInterpolationStringAndWhereWithoutAppealInterpolationString__InvalidOperationException()
        {
            var result = _context.Humans.Select(x => new {interpolation = $"{x.Age}", age = x.Age})
                .Where(x => x.age != 0);
            Assert.DoesNotThrow(() => result.ToList());
        }

        [Test]
        public void
            Interpolation__CallInterpolationStringAndWhereWithoutAppealInserpolationString__InvalidOperationException()
        {
            var result = _context.Humans.Select(x => $"{x.Age}").Where(x => true);
            Assert.DoesNotThrow(() => result.ToList());
        }

        [Test]
        public void
            Interpolation__CallInterpolationStringAndWhereWithAppealInterpolationString__InvalidOperationException()
        {
            var result = _context.Humans.Select(x => $"{x.Age}").Where(x => x != "");
            Assert.Throws<InvalidOperationException>(() => result.ToList());
        }

        [Test]
        public void
            LocalVariableTest__Success()
        {
            var list = Enumerable.Range(1, 100).ToList();
            var result = _context.Humans.Select(x => x.Age).Where(x => list.Contains(x));
            Assert.DoesNotThrow(() => result.ToList());
        }

        [Test]
        public void
            LocalVariableTest__InvalidOperationException()
        {
            var list = Enumerable.Range(1, 100).ToList();
            var result = _context.Humans
                .Where(x => x.simpleList.Any())
                .Select(x => x.Age)
                .Where(x => list.Contains(1));

            var visitor = new Visitor();
            visitor.Visit(result.Expression);
            Assert.Throws<InvalidOperationException>(() => result.ToList());
        }


        [Test]
        public void CreateEntityUseConstructor()
        {
            var result = _context.Humans
                .Select(x => new TestDto(x.Age))
                .Where(x => x.test < 100)
                .ToList();
        }

        [Test]
        public void SelectTest()
        {
            var test = _context.Humans.Select(x => x.simpleList).Select(x=>x);

            var visitor = new Visitor();
            visitor.Visit(test.Expression);
        }

        [Test]
        public void Test123()
        {
            var en1 = Enumerable.Range(1, 10).Select(x =>
            {
                Console.WriteLine(1);

                return x;
            });
            var en2 = Enumerable.Range(1, 10)
                .Select(x =>
                {
                    Console.WriteLine(2);
                    return x;
                });


            var s = en1.Concat(en2).ToList();
        }
    }

    public class TestDto
    {
        public int test { get; set; }

        public TestDto(int test)
        {
            this.test = test;
        }
    }
}