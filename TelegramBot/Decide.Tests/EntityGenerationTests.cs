using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using BotOnStateMachine.Commands;
using Decide.BotStructure.Attributes;
using Decide.CodeGenerator;
using Decide.TelegramBot.UseMes;
using Force;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using NUnit.Framework;
using TestFSharp.Handlers;
using Tests;

namespace Tests
{
    public class EntityGenerationTests
    {
        private VisitorInfoFactory _factory;
        private ReplaceVisitor _replaceVisitor;
        private ReplaceHandler _replaceHandler;
        private TextWriterHandler _textWriterHandler;
        private CreateClassHandler _createClassHandler;

        [SetUp]
        public void Setup()
        {
            _factory = new VisitorInfoFactory();
            _replaceVisitor = new ReplaceVisitor();
            _replaceHandler = new ReplaceHandler(_replaceVisitor);
            _textWriterHandler = new TextWriterHandler();
            _createClassHandler = new CreateClassHandler(_replaceHandler, _textWriterHandler);
        }

        [Test]
        public void ClassGeneratorGenerate__UseMeGenerate__CustomUseMe()
        {
        }

        //../../Decide.BotStructure\UseMes\
        //C:\Users\evgeniy\Documents\telegrambot\TelegramBot\Decide.Tests\EntityGenerationTests.cs
        //C:\Users\evgeniy\Documents\telegrambot\TelegramBot\Decide.BotStructure\UseMes\UseMeImpl.cs

        [Test]
        public void ClassGeneratorGenerate__CommandGenerate__CustomCommand()
        {
            const string insertStr = "NewCommand";
            const string insertAttributeStr = "NewAttribute";
            var useMe = _factory.CommandReplaceInfoCreate(
                new Dictionary<string, string>
                {
                    {nameof(CommandImpl), insertStr},
                    {"\"description\"", insertAttributeStr}
                });
            _createClassHandler.Handle(useMe);
        }

        [Test]
        public void ClassGeneratorGenerate__UseMeGenerate_CustomUseMe()
        {
            var insertStr = "NewUseMe";
            var useMe = _factory.UseMeReplaceInfoCreate(new Dictionary<string, string>()
            {
                {nameof(UseMeImpl), insertStr}
            });
            _createClassHandler.Handle(useMe);
        }


        [Test]
        public void ReplaceVisitorTest()
        {
            var path = "../../../../Decide.Domain/Domain/StateType.cs";
            var code = File.ReadAllText(path);
            var stateType = _factory.StateReplaceInfoCreate(new Dictionary<string, string>()
            {
                {"enum", "public enum StateValue{a,b,c}"}
            });
            _createClassHandler.Handle(stateType);
        }

        [Test]
        public void EnumGeneration__StateTypeEnumGenerate__GetStateType()
        {
        }

        [Test]
        public void UploadModelInSpreadSheetTes1t()
        {
            var b = new B();
            var a = new A();
            var aa = (A) b;
            Console.WriteLine(a is B);
            Console.WriteLine(aa is B);
        }


        public class A
        {
            public string s = "1";
        }


        public class B : A
        {
            public string ss = "11";
        }

        [Test]
        public void ClassWriter__WriteNewImpClass__WriteNewImpClass()
        {
            var b = new B();

            var bb = (A) b;
            var s = bb is B;
        }
    }
}