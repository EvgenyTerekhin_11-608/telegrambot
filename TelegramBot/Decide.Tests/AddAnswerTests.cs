using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using DIContainer;
using Interoperability.CSharp;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using NUnit.Framework;
using TestFSharp;
using TestFSharp.Domain;

namespace Tests
{
    public class AddAnswerTests
    {
        private Pupil _pupil;
        private List<InformaticTask> tasks;

        [SetUp]
        public void Setup()
        {
            new IOC();
            var context = IOC.Container.Get<DbContext, BotDbContext>();

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            var InfTasks = IinformaticTasks();
            context.InformaticTasks.AddRange(InfTasks);
            context.SaveChanges();
        }

        public static List<InformaticTask> IinformaticTasks()
        {
            return Generate((x, i) =>
                new InformaticTask(i + 1, $"Правильный ответ {i}?", i.ToString()));
        }


        public static List<T> Generate<T>(Func<int, int, T> resultFunc, int count = 100)
            => Enumerable.Range(1, count)
                .Select(resultFunc)
                .ToList();

        [Test]
        public void AddAnswer_AddIncorrectAnswers_AllAnswersIsFailure()
        {
            var context = IOC.Container.Get<DbContext, BotDbContext>();

            tasks = context.InformaticTasks.OrderBy(x => x.TaskId).ToList();
            var ans1 = new Answer(1, "1");
            var ans2 = new Answer(2, "123");
            var ans3 = new Answer(3, "213");
            var ans3_1 = new Answer(3, 2.ToString());
            var oneHour = new TimeSpan(0, 1, 0, 0);
            var exam = new Exam(
                Predmet.Informatic,
                DateTime.Now,
                DateTime.Now.Add(oneHour),
                new List<InformaticTask>() {tasks[0], tasks[1], tasks[2]});

            exam.AddAnswer(ans1, context);

            exam.AddAnswer(ans2, context);

            exam.AddAnswer(ans3, context);

            _pupil = new Pupil(1, new EducationCard());
            _pupil.AddExam(exam, context);
            context.Add(_pupil);
            context.SaveChanges();

            exam = context.Pupils
                .Where(x => x.ChatId == _pupil.ChatId)
                .SelectMany(x => x.EdicationCard.Exams)
                .First();

            var value = new AnalyzerHandler(context, exam.Id).Handle();

            Assert.AreEqual(value.F_Count, 3);
        }

        [Test]
        public void AddAnswer_ReAddRightAnswer_OneAnswerCorrect()
        {
            var context = IOC.Container.Get<DbContext, BotDbContext>();

            tasks = context.InformaticTasks.OrderBy(x => x.TaskId).ToList();
            var ans1 = new Answer(1, "1");
            var ans2 = new Answer(2, "123");
            var ans3 = new Answer(3, "213");
            var ans3_1 = new Answer(3, 2.ToString());
            var oneHour = new TimeSpan(0, 1, 0, 0);
            var exam = new Exam(
                Predmet.Informatic,
                DateTime.Now,
                DateTime.Now.Add(oneHour),
                new List<InformaticTask>() {tasks[0], tasks[1], tasks[2]});

            exam.AddAnswer(ans1, context);

            exam.AddAnswer(ans2, context);

            exam.AddAnswer(ans3, context);

            exam.AddAnswer(ans3_1, context);

            _pupil = new Pupil(1, new EducationCard());
            _pupil.AddExam(exam, context);
            context.Add(_pupil);
            context.SaveChanges();

            exam = context.Pupils
                .Where(x => x.ChatId == _pupil.ChatId)
                .SelectMany(x => x.EdicationCard.Exams)
                .First();

            var value = new AnalyzerHandler(context, exam.Id).Handle();

            Assert.AreEqual(value.S_Count, 1);
            Assert.AreEqual(value.F_Count, 2);
        }

        [Test]
        public void AddAnswer_AddAllRightAnswers_AllAnswersCorrect()
        {
            var context = IOC.Container.Get<DbContext, BotDbContext>();

            tasks = context.InformaticTasks.OrderBy(x => x.TaskId).ToList();
            var ans1 = new Answer(1, "0");
            var ans2 = new Answer(2, "1");
            var ans3 = new Answer(3, "2");
            var ans3_1 = new Answer(3, 2.ToString());
            var oneHour = new TimeSpan(0, 1, 0, 0);
            var exam = new Exam(
                Predmet.Informatic,
                DateTime.Now,
                DateTime.Now.Add(oneHour),
                new List<InformaticTask>() {tasks[0], tasks[1], tasks[2]});

            exam.AddAnswer(ans1, context);

            exam.AddAnswer(ans2, context);

            exam.AddAnswer(ans3, context);

            exam.AddAnswer(ans3_1, context);

            _pupil = new Pupil(1, new EducationCard());
            _pupil.AddExam(exam, context);
            context.Add(_pupil);
            context.SaveChanges();


            exam = context.Pupils
                .Where(x => x.ChatId == _pupil.ChatId)
                .SelectMany(x => x.EdicationCard.Exams)
                .First();

            var value = new AnalyzerHandler(context, exam.Id).Handle();

            Assert.AreEqual(value.S_Count, 3);
        }

        [Test]
        public void AddAnswer_AddAnswerBeforeTest_AnswerCountEquals0()
        {
            var context = IOC.Container.Get<DbContext, BotDbContext>();

            tasks = context.InformaticTasks.OrderBy(x => x.TaskId).ToList();
            var ans1 = new Answer(1, "1");
            var ans2 = new Answer(2, "123");
            var ans3 = new Answer(3, "213");
            var ans3_1 = new Answer(3, 2.ToString());
            var oneHour = new TimeSpan(0, 1, 0, 0);
            var exam = new Exam(
                Predmet.Informatic,
                DateTime.Now.Subtract(2 * oneHour),
                DateTime.Now.Subtract(oneHour),
                new List<InformaticTask>() {tasks[0], tasks[1], tasks[2]});

            exam.AddAnswer(ans1, context);

            exam.AddAnswer(ans2, context);

            exam.AddAnswer(ans3, context);

            _pupil = new Pupil(1, new EducationCard());
            _pupil.AddExam(exam, context);
            context.Add(_pupil);
            context.SaveChanges();


            var answerCount = context.Pupils
                .Where(x => x.ChatId == _pupil.ChatId)
                .SelectMany(x => x.EdicationCard.Exams)
                .Select(x => x.Answers)
                .First()
                .Count;


            Assert.AreEqual(answerCount, 0);
        }

        [Test]
        public void AddAnswer_AddAnswerAfterTest_AnswerCountEquals0()
        {
            var context = IOC.Container.Get<DbContext, BotDbContext>();
            tasks = context.InformaticTasks.OrderBy(x => x.TaskId).ToList();
            var ans1 = new Answer(1, "1");
            var ans2 = new Answer(2, "123");
            var ans3 = new Answer(3, "213");
            var ans3_1 = new Answer(3, 2.ToString());
            var oneHour = new TimeSpan(0, 1, 0, 0);
            var exam = new Exam(
                Predmet.Informatic,
                DateTime.Now.Add(oneHour),
                DateTime.Now.Add(2 * oneHour),
                new List<InformaticTask> {tasks[0], tasks[1], tasks[2]});

            exam.AddAnswer(ans1, context);

            exam.AddAnswer(ans2, context);

            exam.AddAnswer(ans3, context);

            _pupil = new Pupil(1, new EducationCard());
            _pupil.AddExam(exam, context);
            context.Add(_pupil);
            context.SaveChanges();

            var answerCount = context.Pupils
                .Where(x => x.ChatId == _pupil.ChatId)
                .SelectMany(x => x.EdicationCard.Exams)
                .Select(x => x.Answers)
                .First()
                .Count;


            Assert.AreEqual(answerCount, 0);
        }

        [Test]
        public void DeleteDb()
        {
            using (var context = new BotDbContext())
            {
                context.Database.EnsureDeleted();
            }
        }
    }
}