using System.ComponentModel.DataAnnotations;
using Decide.Infrastructure.Attributes;
using NUnit.Framework;
using TestFSharp;

namespace Tests
{
    public class TestBaseEntity : BaseEntity
    {
        [Decide.Infrastructure.Attributes.Required]
        [NotNull]
        public string Property { get; set; }

        [NotNull]

        public string Property1 { get; set; }

        [Min(12)]
        [Max(20)]
        public int Property2 { get; set; }

        public TestBaseEntity(string property, string property1, int property2)
        {
            Property = property;
            Property1 = property1;
            Property2 = property2;
            CheckAttributes();
        }
    }

    public class BaseEntityTests
    {
        [Test]
        public void Inherit__InheritBaseEntity__WatchAllPropertyDerived()
        {

            /////////////////////////////////////////
            var s = new TestBaseEntity("s","s",13);
            Assert.NotNull(s);
        }
    }

   
}