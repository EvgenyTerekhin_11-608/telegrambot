using System;
using System.Linq;
using NUnit.Framework;

namespace Tests
{
    public class Iter
    {
        public string Counter { get; set; }
    }

    public class IEnumerableExtentiosn
    {
        [TestCase(10)]
        public void IEnumerableTest(int length)
        {
            var en = Enumerable.Range(1, 1).Select(x => new Iter() {Counter = ""}).ToList();
            var summ = en.Select(x =>
            {
                x.Counter += "a";
                return x;
            });
            for (int i = 0; i < length; i++)
            {
                var s = summ.ToList();
                Console.WriteLine(i + " " + s[0].Counter);
            }

            Console.WriteLine("end");

            var counter = summ.First().Counter;
            Assert.AreEqual(length, summ.First().Counter);
        }

        [Test]
        public void Foreach__ForeachImitationTOList()
        {
            var s = Enumerable.Range(1, 10).Select(x =>
            {
                Console.WriteLine(x);
                return x;
            });
        }
    }
}