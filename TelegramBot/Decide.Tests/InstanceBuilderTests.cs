using System;
using System.Collections.Generic;
using Decide.InstanceBuilder;
using NUnit.Framework;

namespace Tests
{
    public class InstanceBuilderTests
    {
        public class Handler1
        {
        }

        public class Handler
        {
        }

        public class TestClass
        {
            private readonly Handler _handler;
            private readonly Handler1 _handler1;

            public TestClass(Handler handler, Handler1 handler1)
            {
                _handler = handler;
                _handler1 = handler1;
            }

            public string Print(string s)
            {
                return s;
            }
        }

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetExpressionCtor__GetInstanceWithout__GetLambda()
        {
            var t = typeof(TestClass);
            var parameters = new List<Type>() {typeof(Handler1), typeof(Handler)};
            var instanceBuilder = new LambdaBuilder();
            var result = instanceBuilder.GetExpressionCtor(t);
            var obj = result.Compile().DynamicInvoke(new Handler(), new Handler1());
            var castObj = (TestClass) obj;
            Assert.AreEqual(castObj.Print("sosi"), "sosi");
        }

        [Test]
        public void GetExpressionCtor_GetExpressionWithParameters_GetLambda()
        {
            var t = typeof(TestClass);
            var instanceBuilder = new LambdaBuilder();
            var result = instanceBuilder.GetExpressionCtor(t, new List<Type> {typeof(Handler), typeof(Handler1)});
            var obj = result.Compile().DynamicInvoke(new Handler(), new Handler1());
            var castObj = (TestClass) obj;
            Assert.AreEqual(castObj.Print("sosi"), "sosi");
        }

        [Test]
        public void GetExpressionCtor_GetExpressionWithParametersNotOrder_GetLambda()
        {
            var t = typeof(TestClass);
            var instanceBuilder = new LambdaBuilder();
            var result = instanceBuilder.GetExpressionCtor(t, new List<Type> {typeof(Handler1), typeof(Handler)});
            var obj = result.Compile().DynamicInvoke(new Handler(), new Handler1());
            var castObj = (TestClass) obj;
            Assert.AreEqual(castObj.Print("sosi"), "sosi");
        }

        [Test]
        public void Test()
        {
            var t = typeof(TestClass);
            var Lambdauilder = new LambdaBuilder();
            var InstanceBuilder = new InstanceBuilder(Lambdauilder);
            var res = InstanceBuilder.Build<TestClass>(t, new object[] {new Handler1(), new Handler()});
            Assert.AreEqual(res.Print("sosi"), "sosi");
        }
    }
}