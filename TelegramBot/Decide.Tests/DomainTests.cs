using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyModel;
using NUnit.Framework;
using TestFSharp;
using TestFSharp.Handlers;

namespace Tests
{
    public class DomainTests
    {
        [Test]
        public void DomainTest()
        {
            var sss = AppDomain.CurrentDomain.BaseDirectory;
            var assembliesName = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll");
            var s123 = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(x=>x.FullName.Contains("Decide"))
                .ToList();
            var s = Assembly.Load(new AssemblyName("Decide.Domain")).OneToList()
                .Concat(Assembly.Load(new AssemblyName("Decide.NotifyModule")).OneToList());
            var ss = s.SelectMany(x => x.ExportedTypes).Where(predicate).ToList();
            Assert.True(ss.Any());
        }

        [Test]
        public void CountAssemblies()
        {
            var s = AppDomain.CurrentDomain.GetAssemblies().Length;
            var ss = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x=>x.ExportedTypes);
        }
        private Func<Type, bool> predicate = y =>
            y.Name == "NotificationHandlerInfo";
    }
}