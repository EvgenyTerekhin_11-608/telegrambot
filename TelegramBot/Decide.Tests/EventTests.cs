using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Decide.EventsAnalyzer;
using Microsoft.EntityFrameworkCore.Query.ExpressionTranslators.Internal;
using Microsoft.EntityFrameworkCore.Query.ExpressionVisitors.Internal;
using Microsoft.VisualStudio.TestPlatform.Common.Utilities;
using NUnit.Framework;
using TestFSharp;

namespace Tests
{
   

    public class EventTests
    {
        private DateTime Now = DateTime.Now;
        private TimeSpan Second_10 = new TimeSpan(0, 0, 0, 10);
        private TimeSpan Second_20 = new TimeSpan(0, 0, 0, 20);
        private TimeSpan Second_30 = new TimeSpan(0, 0, 0, 30);
        private TimeSpan Second_1 = new TimeSpan(0, 0, 0, 1);
        private TimeSpan Second_5 = new TimeSpan(0, 0, 0, 5);
        private TimeSpan Second_2 = new TimeSpan(0, 0, 0, 2);
        private TimeSpan Minute = new TimeSpan(0, 0, 0, 60);

        [Test]
        public void SimpleEventTest__Second_5()
        {
            var event1 = new Event(DateTime.Now.Add(Second_5),
                () => { Console.WriteLine(1); },1);
            var event2 = new Event(DateTime.Now.Add(Second_5).Add(Second_5),
                () => { Console.WriteLine(2); },2);
            var event3 = new Event(DateTime.Now.Add(Second_5).Add(Second_5).Add(Second_5),
                () => { Console.WriteLine(3); },3);
            var event4 = new Event(DateTime.Now.Add(Second_5).Add(Second_5).Add(Second_5).Add(Second_5),
                () => { Console.WriteLine(4); },4);

            EventAnalyzer.Add(event4);
            EventAnalyzer.Add(event3);
            EventAnalyzer.Add(event2);
            EventAnalyzer.Add(event1);

            Thread.Sleep(21000);
        }

        [Test]
        public void SimpleEventTest__Second_20()
        {
            var event1 = new Event(DateTime.Now.Add(Second_30),
                () => { Console.WriteLine(1); },1);
            var event2 = new Event(DateTime.Now.Add(Second_30).Add(Second_30),
                () => { Console.WriteLine(2); },2);
            var event3 = new Event(DateTime.Now.Add(Second_30).Add(Second_30).Add(Second_30),
                () => { Console.WriteLine(3); },3);
            var event4 = new Event(DateTime.Now.Add(Second_30).Add(Second_30).Add(Second_30).Add(Second_30),
                () => { Console.WriteLine(4); },4);

            EventAnalyzer.Add(event4);
            EventAnalyzer.Add(event3);
            EventAnalyzer.Add(event2);
            EventAnalyzer.Add(event1);

            Thread.Sleep(81000);
        }

    


        
    }
}