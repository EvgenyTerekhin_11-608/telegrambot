using System;
using System.Net;
using BotOnStateMachine.Commands;
using Decide.TelegramBot.BaseClasses;
using DIContainer;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Telegram.Bot;
using TestFSharp;

namespace Tests
{
    public interface IA
    {
        string GetString();
    }

    public class A : IA
    {
        private readonly int _key;

        public A(int key)
        {
            _key = key;
        }


        public string GetString()
        {
            return _key.ToString();
        }
    }

    public class RegisterTests
    {
        public interface ISimpleServiceA
        {
            string GetString();
        }

        public class SimpleServiceA : ISimpleServiceA
        {
            public string GetString()
            {
                return "SimpleServiceA";
            }
        }

        public interface IService : ISimpleServiceA
        {
            ServiceB _serviceB { get; set; }
        }

        public class ServiceA : IService
        {
            public ServiceB _serviceB { get; set; }

            public ServiceA(ServiceB service)
            {
                _serviceB = service;
            }


            public string GetString()
            {
                return "ServiceA";
            }
        }

        public interface IServiceB
        {
        }

        public class ServiceB : IServiceB
        {
            public ServiceB(ServiceC serviceC)
            {
            }
        }

        public class ServiceC : IServiceC
        {
        }

        public interface IServiceC
        {
        }

        public interface IUseValueEntity
        {
        }

        public class UseValueEntity : IUseValueEntity
        {
        }

        public class TestUseValueConstructorEntity : ITestUseValueConstructorEntity
        {
            private readonly IUseValueEntity _useValueEntity;

            public TestUseValueConstructorEntity(IUseValueEntity useValueEntity)
            {
                _useValueEntity = useValueEntity;
            }

            public string GetString() => "TestUseValueConstructorEntity";
        }

        public interface ITestUseValueConstructorEntity
        {
            string GetString();
        }

        private Container Container { get; set; }

        [SetUp]
        public void Start()
        {
            Container = new Container();
        }

        [Test]
        public void Register__RegisterSimpleInterface__GetImplInterface()
        {
            Container.Register<ISimpleServiceA, SimpleServiceA>();
            var service = Container.Get<ISimpleServiceA>();
            Assert.AreEqual(service.GetString(), new SimpleServiceA().GetString());
        }

        [Test]
        public void Register__RegisterSimpleClass__GetSimpleClass()
        {
            Container.Register<SimpleServiceA, SimpleServiceA>();
            var service = Container.Get<SimpleServiceA>();
            Assert.AreEqual(service.GetString(), new SimpleServiceA().GetString());
        }

        [Test]
        public void Register__RegisterInterfacesWithSingletonLifeTime__GetSingletonValue()
        {
            Container.Register<IServiceC, ServiceC>(LifeTime.Singleton);
            Container.Register<IServiceB, ServiceB>(LifeTime.Singleton);
            Container.Register<IService, ServiceA>(LifeTime.Singleton);
            var service = Container.Get<IService>();
            var serviceB = Container.Get<IServiceB>();
            Assert.AreEqual(serviceB.GetHashCode(), service._serviceB.GetHashCode());
            Assert.AreEqual(service.GetString(), new ServiceA(new ServiceB(new ServiceC())).GetString());
        }

        [Test]
        public void Register__RegisterInterfaceWithSingletonLifeTimeAndCtorParametersTransient__GetSingletonValue()
        {
            Container.Register<IServiceC, ServiceC>(LifeTime.Transient);
            Container.Register<IServiceB, ServiceB>(LifeTime.Transient);
            Container.Register<IService, ServiceA>(LifeTime.Singleton);
            var serviceA = Container.Get<IService>();
            var service_A = Container.Get<IService>();
            var serviceC = Container.Get<IServiceC>();
            var service_C = Container.Get<IServiceC>();

            Assert.AreEqual(serviceA.GetString(), new ServiceA(new ServiceB(new ServiceC())).GetString());
            Assert.AreEqual(service_A.GetHashCode(), serviceA.GetHashCode());
            Assert.AreNotEqual(service_C.GetHashCode(), serviceC.GetHashCode());
        }


        //FIX IT
        [Test]
        public void
            Register__RegisterInterfaceWithSingletonLifeTimeAndCtorParametersTransientAndScoped__GetSingletonValue()
        {
            Container.Register<IServiceC, ServiceC>(LifeTime.Transient);
            Container.Register<IServiceB, ServiceB>();
            Container.Register<IService, ServiceA>(LifeTime.Singleton);
            var serviceA = Container.Get<IService>();
            var service_A = Container.Get<IService>();
            var serviceC = Container.Get<IServiceC>();
            var service_C = Container.Get<IServiceC>();
            Assert.AreEqual(serviceA.GetString(), new ServiceA(new ServiceB(new ServiceC())).GetString());
            Assert.AreEqual(service_A.GetHashCode(), serviceA.GetHashCode());
            Assert.AreNotEqual(service_C.GetHashCode(), serviceC.GetHashCode());
        }

        [Test]
        public void Register__RegisterInterfacesWithLifeTimeTransient__GetTransientValue()
        {
            Container.Register<IServiceC, ServiceC>(LifeTime.Transient);
            Container.Register<IServiceB, ServiceB>(LifeTime.Transient);
            Container.Register<IService, ServiceA>(LifeTime.Transient);
            var a = Container.Get<IService>();
            var b = Container.Get<IService>();
            Assert.AreNotEqual(a.GetHashCode(), b.GetHashCode());
        }

        [Test]
        public void Register__TInAndOut__GetValue()
        {
            Container.Register<IServiceC, ServiceC>(LifeTime.Transient);
            Container.Register<IServiceB, ServiceB>();
            Container.Register<ServiceA>(LifeTime.Singleton);
            var ex1 = Container.Get<ServiceA>();
            var ex11 = Container.Get<ServiceA>();
            Assert.AreEqual(ex1.GetHashCode(), ex11.GetHashCode());
        }

        [Test]
        public void Register__DependensesItsUseValue__GetValue()
        {
            IOC.Container.Register<DbContext, BotDbContext>(new BotDbContext());
            IOC.Container.Register<IDataSet, DataSet>();
        }


        [Test]
        public void Get__RegisterUseValue__GetValue()
        {
            IOC.Container.Register<DbContext, BotDbContext>(new BotDbContext());
            Assert.DoesNotThrow(()=>IOC.Container.Get<DbContext, BotDbContext>());
            
        }
        [Test]
        public void Register__TestReWriteRegisterValue__GetValue()
        {
            IOC.Container.Register<IA, A>(new A(1));
            var value = IOC.Container.Get<IA, A>().GetString();
            IOC.Container.Register<IA, A>(new A(2));
            var value1 = IOC.Container.Get<IA, A>().GetString();

            Assert.AreNotEqual(value, value1);
        }

        [Test]
        public void Register__UseValueRegisterAndRegisterEntityWithCtorParametersUseValueEntity__Correct()
        {
            IOC.Container.Register<IUseValueEntity,UseValueEntity>(new UseValueEntity());
            IOC.Container.Register<ITestUseValueConstructorEntity,TestUseValueConstructorEntity>();
            var testEntity = IOC.Container.Get<ITestUseValueConstructorEntity>();
            Assert.AreEqual(testEntity.GetString(),"TestUseValueConstructorEntity");
        }
    }
}