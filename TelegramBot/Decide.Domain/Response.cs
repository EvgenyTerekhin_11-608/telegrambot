namespace TestFSharp.Infrastructure
{
    public class Response
    {
        public bool IsSuccess { get; set; }
        public object Data { get; set; }

        public static Response Success(object data)
        {
            return new Response
            {
                Data = data,
                IsSuccess = true
            };
        }

        public static Response Error(object data)
        {
            return new Response
            {
                Data = data,
                IsSuccess = false
            };
        }
    }
}