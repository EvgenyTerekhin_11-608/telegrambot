using System;
using System.Collections.Generic;

namespace TestFSharp.Domain
{
    public class InformaticExamTest : ExamTestBase
    {
        public IEnumerable<InformaticTask> InformaticTasks { get;  private set;}
        public DateTime UpdateDate { get;  private set;}
    }
}