namespace Telegram_Bot_Machine.Models
{
public enum StateType
{
    UnAuth,
    Auth,
    Exam,
    ExamSetup,
    ExamPlanning,
    ChangeNumberQuestions,
    ChangeTimeDuration,
    BackToTask,
    EndExam,
    BackToTaskInEndExam,
    Лист11,
    Global
}}