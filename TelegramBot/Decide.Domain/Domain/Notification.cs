using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TestFSharp.Domain
{
    public class Notification
    {
        public int Id { get; private set; }
        public NotificationHandlerInfo MethodInfo { get; private set; }

        public string TInHandlerGeneric { get; private set; }
        public DateTime ActionDate { get; private set; }

        public Notification(NotificationHandlerInfo methodInfo, DateTime actionDate, string tInHandlerGenericName)
        {
            MethodInfo = methodInfo;
            ActionDate = actionDate;
            TInHandlerGeneric = tInHandlerGenericName;
        }

        protected Notification()
        {
        }
    }

    public class NotificationHandlerInfo
    {
        public NotificationHandlerInfo(object valuesArgumentsType)
        {
            ValueArgumentsType = JsonConvert.SerializeObject(valuesArgumentsType);
        }

        private NotificationHandlerInfo()
        {
        }

        public string ValueArgumentsType { get; private set; }
    }
}