using System;
using System.Collections.Generic;
using System.Linq;
using Force.Ddd;
using TestFSharp.Infrastructure;

namespace TestFSharp.Domain
{
    public class Exam
    {
        public int Id { get; set; }

        public Exam(
            Predmet predmet,
            DateTime dateStart,
            DateTime dateEnd,
            List<InformaticTask> InformaticTasks)
        {
            this.InformaticTasks = InformaticTasks;
            Predmet = predmet;
            Answers = new List<Answer>();
            DateStart = dateStart;
            DateEnd = dateEnd;
        }

        public Predmet Predmet { get; private set; }

        public virtual List<InformaticTask> InformaticTasks { get; private set; }
        public virtual List<Answer> Answers { get; private set; }
        public DateTime DateStart { get; private set; }
        public DateTime DateEnd { get; private set; }

        public Response AddAnswer(Answer answer, BotDbContext context)
        {
            if (IsFutureExam.IsSatisfiedBy(this))
                return Response.Error("Еще будет");
            if (IsLastExam.IsSatisfiedBy(this))
                return Response.Error("Уже прошло");

            if (Answers.Any(x => x.InformaticTaskId == answer.InformaticTaskId))
            {
                Answers.RemoveAll(x => x.InformaticTaskId == answer.InformaticTaskId);
                context.Add(answer);
                Answers.Add(answer);
                context.SaveChanges();
                return Response.Success("Ответ изменен");
            }

            Answers.Add(answer);
            context.Add(answer);
            context.SaveChanges();
            return Response.Success("Успешно");
        }

        private Exam()
        {
        }

        public static Spec<Exam> IsFutureExam =>
            new Spec<Exam>(x => x.DateStart > DateTime.Now && x.DateEnd > DateTime.Now);

        public static Spec<Exam> IsProcessExam =>
            new Spec<Exam>(x => x.DateStart < DateTime.Now && x.DateEnd > DateTime.Now);

        public static Spec<Exam> IsLastExam =>
            new Spec<Exam>(x => x.DateStart < DateTime.Now && x.DateEnd < DateTime.Now);
    }
}