using System.Collections.Generic;
using System.Linq;

namespace Domain.Domain
{
    public class Human
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        public List<int> simpleList { get; set; }
        public string Info { get; set; }

        public
            Human(string name, int age, string info)
        {
            simpleList = Enumerable.Range(1, 100).ToList();
            Name = name;
            Age = age;
            Info = info;
        }
    }
}