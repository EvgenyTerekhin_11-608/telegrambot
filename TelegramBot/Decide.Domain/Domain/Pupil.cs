using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Telegram_Bot_Machine.Models;

namespace TestFSharp.Domain
{
    public class Pupil
    {
        public int ChatId { get; private set; }
        public EducationCard EdicationCard { get; private set; }
        public int EducationCardId { get; private set; }
        public List<Notification> Notifications { get; private set; }

        public PupilState PupilState { get; set; }

        public Pupil(int chatId, EducationCard edicationCard)
        {
            PupilState = PupilState ?? new PupilState();
            ChatId = chatId;
            EdicationCard = edicationCard;
            Notifications = new List<Notification>();
        }

        private Pupil()
        {
        }

        public void AddExam(Exam exam, BotDbContext context)
        {
            EdicationCard.AddExam(exam);
            context.Add(exam);
            context.SaveChanges();
        }

        public void AddNotification(Notification notification, BotDbContext context)
        {
            Notifications.Add(notification);
            context.Add(notification);
            context.SaveChanges();
        }

        public void ChangeState(StateType stateType, BotDbContext botDbContext)
        {
            PupilState.State = stateType;
            botDbContext.SaveChanges();
        }
    }
}