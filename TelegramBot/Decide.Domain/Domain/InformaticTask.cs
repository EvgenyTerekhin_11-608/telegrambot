using System.Collections.Generic;
using System.Xml;

namespace TestFSharp.Domain
{
    public class InformaticTask
    {
        public int TaskId { get; private set; }
        public string QuestionText { get; private set; }
        public byte[] QuestionImageBytes { get; private set; }
        public string AnswerText { get; private set; }
        public byte[] AnswerImagesBytes { get; private set; }


        public InformaticTask(
            int taskId,
            string questionText,
            string answerText,
            byte[] answerImagesBytes = null,
            byte[] questionImageBytes = null)
        {
            TaskId = taskId;
            QuestionText = questionText;
            QuestionImageBytes = questionImageBytes;
            AnswerText = answerText;
            AnswerImagesBytes = answerImagesBytes;
        }

        private InformaticTask()
        {
        }
    }
}