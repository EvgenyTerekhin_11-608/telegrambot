using System;
using System.Linq;

namespace TestFSharp.Domain
{

    public class Test
    {
        public Test()
        {
            
        }


        public void test()
        {
            var ss = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(x=>x.FullName.Contains("Decide"))
                .ToList();
        }
    }
    //ValueType
    public class Answer
    {
        public int Id { get;  private set; }
        public int InformaticTaskId { get; private set; }
        public InformaticTask Task { get;  private set;}
        public string AnswerString { get;  private set;}

        public Answer(InformaticTask task, string answerString)
        {
            Task = task;
            AnswerString = answerString;
        }
        public Answer(int taskId, string answerString)
        {
            InformaticTaskId = taskId;
            AnswerString = answerString;
        }


        protected Answer()
        {
        }
    }
}