using System.Collections.Generic;
using System.Linq;

namespace TestFSharp.Domain
{
    public class EducationCard
    {
        public int Id { get; private set; }
        public virtual List<Exam> Exams { get;private set; }

        public EducationCard()
        {
            Exams = new List<Exam>();
        }

        public void AddExam(Exam ex)
        {
            Exams.Add(ex);
        }
    }
}