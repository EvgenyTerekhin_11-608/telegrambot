using System;
using System.Linq;
using System.Xml.Linq;
using Decide.AppSettings;
using Domain.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using TestFSharp.Domain;
using TestFSharp.Infrastructure;

namespace TestFSharp
{
    public class BotDbContext : DbContext
    {
        public BotDbContext()
        {
        }

        public DbSet<EducationCard> EducationCards { get; set; }
        public DbSet<InformaticTask> InformaticTasks { get; set; }
        public DbSet<Pupil> Pupils { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Notification> Notifitions { get; set; }
        public DbSet<PupilState> PupilStates { get; set; }
        public DbSet<Human> Humans { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(MyLoggerFactory)
                .UseNpgsql(AppSettingsProvider.GetSetting<DomainSettings>("Decide.Domain").ConnectionString)
                .ConfigureWarnings(warnings => warnings.Throw(RelationalEventId.QueryClientEvaluationWarning))
                ;
        }

        public static readonly LoggerFactory MyLoggerFactory
            = new LoggerFactory(new[] {new ConsoleLoggerProvider((_, __) => true, true)});

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InformaticTask>().HasKey(x => x.TaskId);
            modelBuilder.Entity<Pupil>().HasKey(x => x.ChatId);
            modelBuilder.Entity<Notification>().OwnsOne(x => x.MethodInfo,
                s => { s.Property(p => p.ValueArgumentsType).HasColumnName("ValuesArgumentsType"); });
            modelBuilder.Entity<Pupil>().OwnsOne(x => x.PupilState,
                s => { s.Property(p => p.State).HasColumnName("PupilState"); });
            base.OnModelCreating(modelBuilder);
        }
    }
}