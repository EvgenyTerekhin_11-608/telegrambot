using System;
using NotifyModule.Atttibutes;

namespace TestFSharp
{
    public enum StringType
    {
        [TypeConverter(typeof(string))] String,
        [TypeConverter(typeof(int))] Int,
        [TypeConverter(typeof(DateTime))] DateTime,
        [TypeConverter(typeof(double))] Double,
        [TypeConverter(typeof(bool))] Bool
    }
}