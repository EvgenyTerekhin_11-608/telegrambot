using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NotifyModule.Atttibutes
{
    public interface IValueConverter
    {
        object Convert(string value);
    }
}