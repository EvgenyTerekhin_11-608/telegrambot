using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Decide.CodeGenerator;
using Decide.SpreadSheetProvider;
using Force;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json;
using Remotion.Linq.Clauses.ResultOperators;
using TestFSharp;

namespace Decide.TelegramBot
{
    public interface IUploadModelInSpreadSheet
    {
        StateMachineInfo Handle();
    }

    public class WriteUploadModelInSpreadSheetProxy : IUploadModelInSpreadSheet
    {
        private readonly IUploadModelInSpreadSheet _uploadModelInSpreadSheet;
        private readonly IHandler<WriteFileInfo> _writer;

        public WriteUploadModelInSpreadSheetProxy(IUploadModelInSpreadSheet uploadModelInSpreadSheet,
            IHandler<WriteFileInfo> writer)
        {
            _uploadModelInSpreadSheet = uploadModelInSpreadSheet;
            _writer = writer;
        }

        public static string PathToSpreadSheetFile =
            @"../Decide.StateStructureBuild/GoogleSheetInfo/GoogleSheetInfo.json";

        public StateMachineInfo Handle()
        {
            var result = _uploadModelInSpreadSheet.Handle();
            var json_string = JsonConvert.SerializeObject(result);
            _writer.Handle(new WriteFileInfo(PathToSpreadSheetFile, json_string));
            return result;
        }
    }

    public class ReadUploadModelInSpreadSheetProxy : IUploadModelInSpreadSheet
    {
        private readonly IHandler<ReadFileInfo, string> _readWriterHandler;

        public ReadUploadModelInSpreadSheetProxy(IHandler<ReadFileInfo, string> readWriterHandler)
        {
            _readWriterHandler = readWriterHandler;
        }

        public StateMachineInfo Handle()
        {
            var result =
                JsonConvert.DeserializeObject<StateMachineInfo>(_readWriterHandler.Handle(
                    new ReadFileInfo(WriteUploadModelInSpreadSheetProxy.PathToSpreadSheetFile)));
            return result;
        }
    }


    public class UploadModelInSpreadSheet : IUploadModelInSpreadSheet
    {
        private readonly IGoogleSheetApi _api;

        public     UploadModelInSpreadSheet(IGoogleSheetApi api)
        {
            _api = api;
        }

        public StateMachineInfo Handle()
        {
            //todo refactoring
            var allList = _api.GetAllListNames();
            var result = allList.Select(x => new
                {
                    name = x,
                    commands = _api.GetListInfo(x).Skip(1)
                        .Select(xx => Setting.Make(xx.IfLessToAdd(6).Select<string, IEnumerable<string>>((xxx, i) =>
                        {
                            //description,contains,toState
                            if (i == 5 || i == 3 || i == 4)
                                return xxx?.OneToList();
                            return xxx?.Split(',');
                        })))
                        .ToList()
                })
                .Select(x => new UploadingModel(x.name, x.commands)).ToList();


            return new StateMachineInfo(result);
        }
    }
}