using System.Collections.Generic;

namespace Decide.TelegramBot
{
    public class UploadingModel
    {
        public UploadingModel(string stateName, List<Setting> stateSettings)
        {
            StateName = stateName;
            StateSettings = stateSettings;
        }

        public string StateName { get; set; }
        public List<Setting> StateSettings { get; set; }
    }
}