using System.Collections.Generic;

namespace Decide.TelegramBot
{
    public class StateMachineInfo
    {
        public StateMachineInfo(List<UploadingModel> uploadingModels)
        {
            UploadingModels = uploadingModels;
        }

        public List<UploadingModel> UploadingModels { get; set; }
    }
}