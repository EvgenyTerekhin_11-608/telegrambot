using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TestFSharp;

namespace Decide.TelegramBot
{
    public class Setting
    {
        public Setting()
        {
        }

        public List<string> Commands { get; set; }
        public List<string> UseMes { get; set; }
        public List<string> Validators { get; set; }
        public string Contains { get; set; }
        public string ToState { get; set; }

        public string Description { get; set; }

        public Setting(
            List<string> commands, List<string> useMes, List<string> validators,
            string contains, string toState, string description)
        {
            Commands = commands;
            UseMes = useMes;
            Validators = validators;
            Contains = contains;
            ToState = toState;
            Description = description;
        }

        public static Setting Make(IEnumerable<IEnumerable<string>> list)
        {
            var arguments = list.Select(x => x?.ToList()).ToList();
            var commands = arguments[0];
            var usemes = arguments[1];
            var validators = arguments[2];
            var containses = arguments[3]?.FirstOrDefault() ?? string.Empty;
            var toStates = arguments[4]?.FirstOrDefault() ?? string.Empty;
            var description = arguments[5]?.FirstOrDefault() ?? string.Empty;
            var entity =
                new Setting(commands, usemes, validators, containses, toStates, description);

            return entity;
        }
    }
}