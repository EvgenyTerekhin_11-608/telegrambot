using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BotOnStateMachine;
using BotOnStateMachine.Commands;
using Decide.BotStructure;
using Decide.TelegramBot;
using DIContainer;
using Force;
using Microsoft.EntityFrameworkCore;
using TelegramStateMachine.CustomValidators;
using TelegramStateMachine.Infrastructure.Validators;
using Telegram_Bot_Machine.Models;
using TestFSharp;

namespace Decide.StateStructureBuild
{
    public class StateNameDto
    {
        public string StateInfo { get; set; }
    }

    public class StateFactory : IHandler<StateNameDto, State>
    {
        private IUploadModelInSpreadSheet _uploadModelInSpreadSheet;
        private IHandler<Func<Type, bool>, IEnumerable<Type>> _getTypeHandler;
        private StateMachineInfo StateMachineInfo { get; set; }

        public StateFactory(
            IUploadModelInSpreadSheet uploadModelInSpreadSheet,
            IHandler<Func<Type, bool>, IEnumerable<Type>> getTypeHandler)
        {
            _uploadModelInSpreadSheet = uploadModelInSpreadSheet;
            _getTypeHandler = getTypeHandler;
            StateMachineInfo = _uploadModelInSpreadSheet.Handle();
        }

        private UseMe DefaultUseMe = new DefaultUseMe(null);
        private PredicateValidator DefaultPredicateValidator = new TrueValidator();
        private T GetEntities<T>(string typeName)
        {
            Func<string, Func<Type, bool>> predicate = str => type => type.Name == str;
            var result = (T) IOC.Container.Get(_getTypeHandler.Handle(predicate(typeName)).Single());
            return result;
        }

        public State Handle(StateNameDto input)
        {
            var stateSetttings = StateMachineInfo.UploadingModels.Single(x => x.StateName == input.StateInfo)
                .StateSettings;
            var result = stateSetttings.Select(x =>
            {
                var commands = x.Commands.Select(GetEntities<Command>).ToList();
                var usemes = x.UseMes?.Select(GetEntities<UseMe>).ToList() ?? DefaultUseMe.OneToList();
                var validators = x.Validators?.Select(xx => GetEntities<PredicateValidator>($"{xx}Validator"))
                    .ToList() ?? DefaultPredicateValidator.OneToList();
                var contains = new ContainsValidator(x.Contains);
                var transactionManager = new TransactionManager(Enum.Parse<StateType>(x.ToState),
                    IOC.Container.Get<IAppSessionService>(), IOC.Container.Get<DbContext, BotDbContext>());
                return new CommandBase(usemes, commands, validators, contains, transactionManager);
            }).ToList();
            return new State(result);
        }
    }
}