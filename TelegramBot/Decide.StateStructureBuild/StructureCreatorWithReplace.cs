using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using BotOnStateMachine.Commands;
using Decide.CodeGenerator;
using Decide.CodeGenerator.Generator;
using Decide.SpreadSheetProvider;
using Decide.TelegramBot.UseMes;
using Force;
using Microsoft.CodeAnalysis;
using Telegram_Bot_Machine.Models;
using Tests;

namespace Decide.TelegramBot
{
    public interface IStructureCreator
    {
        void Create();
    }

    public class StructureCreatorWithReplace : IStructureCreator
    {
        private readonly CreateClassHandler _createClassHandler;
        private readonly IHandler<EnumValuesInfo, SyntaxTree> _enumGenerator;
        private readonly IVisitorAndFileInfoFactory _factory;
        private IUploadModelInSpreadSheet UploadModelInSpreadSheet { get; }

        public StructureCreatorWithReplace(
            CreateClassHandler CreateClassHandler,
            IHandler<EnumValuesInfo, SyntaxTree> enumGenerator,
            IUploadModelInSpreadSheet uploadModelInSpreadSheet,
            IVisitorAndFileInfoFactory factory)
        {
            _createClassHandler = CreateClassHandler;
            _enumGenerator = enumGenerator;
            _factory = factory;
            UploadModelInSpreadSheet = uploadModelInSpreadSheet;
            _dic =
                new Dictionary<Func<Setting, Dictionary<string, string>>,
                    Func<Dictionary<string, string>, VisitorAndFileInfo>>()
                {
                    {
                        x => x.UseMes?.ToDictionary(xx => nameof(UseMeImpl), xx => xx),
                        x => _factory.UseMeReplaceInfoCreate(x)
                    },
                    {
                        x =>
                        {
                            var dic = x.Commands?.ToDictionary(xx => nameof(CommandImpl), xx => xx);
                            dic?.Add("\"description\"", x.Description);
                            return dic;
                        },
                        x => _factory.CommandReplaceInfoCreate(x)
                    },
//                    {x => x.Contains, newStr => throw new NotImplementedException()},
//                    {x => x.Validator, newStr => throw new NotImplementedException()},
//                    {x => x.Description, newStr => throw new NotImplementedException()}
                };
        }


        private Dictionary<Func<Setting, Dictionary<string, string>>,
            Func<Dictionary<string, string>, VisitorAndFileInfo>> _dic;

        public void Create()
        {
            var data = UploadModelInSpreadSheet.Handle();
            var s =
                _dic.Where(x => x.Key.GetType() != typeof(NotImplementedException))
                    .SelectMany(x =>
                        data.UploadingModels.SelectMany(xx => xx.StateSettings)
                            .Where(xxx => x.Key(xxx) != null && x.Key(xxx).Values.All(xx => xx != null))
                            .Select(xxx => x.Value(x.Key(xxx))))
                    .ToList();
            s.ForEach(x => _createClassHandler.Handle(x));


            var states = data.UploadingModels.Select(x => x.StateName);
            var @enum = _enumGenerator.Handle(new EnumValuesInfo(nameof(StateType), states.ToList()));
            var stateReplace = _factory.StateReplaceInfoCreate(new Dictionary<string, string>
            {
                {nameof(StateType), @enum.ToString()}
            });
            _createClassHandler.Handle(stateReplace);
        }
    }
}