﻿using System;
using Decide.CodeGenerator;
using Decide.CodeGenerator.Generator;
using Decide.SpreadSheetProvider;
using Decide.TelegramBot;
using DIContainer;
using Force;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Tests;

namespace Decide.StateStructureBuild
{
    class Program
    {
        static void Main(string[] args)
        {
            IOC.Container.Register<IGoogleSheetApi, GoogleSheetApi>();
            IOC.Container.Register<IVisitorAndFileInfoFactory, VisitorInfoFactory>();
            IOC.Container.Register<IUploadModelInSpreadSheet, UploadModelInSpreadSheet>();
            IOC.Container.Register<CSharpSyntaxRewriter, ReplaceVisitor>();
            IOC.Container.Register<IHandler<VisitorInfo, string>, ReplaceHandler>();
//            IOC.Container.Register<IHandler<WriteFileInfo>,TextWriterHandler>();
            IOC.Container.Register<IHandler<WriteFileInfo>, SaveTextWriterHandle>(
                new SaveTextWriterHandle(new ReadWriterHandler(), new TextWriterHandler()));
            IOC.Container.Register<IHandler<VisitorAndFileInfo>, CreateClassHandler>();
            IOC.Container.Register<IHandler<EnumValuesInfo, SyntaxTree>, EnumGenerator>();
            IOC.Container.Register<IStructureCreator, StructureCreatorWithReplace>();
            var creator = IOC.Container.Get<IStructureCreator>();
            creator.Create();
        }
    }
}