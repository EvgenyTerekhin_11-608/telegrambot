using System.Collections.Generic;
using System.Net;
using BotOnStateMachine;
using BotOnStateMachine.Commands;
using Decide.AppSettings;
using Decide.BotStructure;
using Decide.StateStructureBuild;
using DIContainer;
using Force;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;
using TestFSharp;

namespace Decide.TelegramBot
{
    public class TelegramBotManager
    {
        private static TelegramBotClient _client;

        public TelegramBotManager()
        {
            var appset = AppSettingsProvider.GetSetting<TelegramBotSettings>("Decide.TelegramBot");
            var token = appset.TelegramToken;
            var wp = new WebProxy(appset.ProxyInfo.Address);
            wp.Credentials = new NetworkCredential(appset.ProxyInfo.Credentials.Username,
                appset.ProxyInfo.Credentials.Password
            );
            _client = new TelegramBotClient(token, wp);
            _client.OnUpdate += onUpdate;
            _client.StartReceiving();
            while (true)
            {
            }
        }

        private void onUpdate(object sender, UpdateEventArgs e)
        {

//            var s = IOC.Container.Get<ISender, Sender>().Send(".", new InlineKeyboardMarkup(InlineKeyboardButton.WithCallbackData()));
            var chatId = e.Update.Message?.From?.Id ?? e.Update.CallbackQuery.From.Id;
            var context = new BotDbContext();
            IOC.Container.Register<DbContext, BotDbContext>(context);
            IOC.Container.Register<IAppSessionService, AppSessionService>(new AppSessionService(context, chatId));
            IOC.Container.Register<ISender, Sender>(new Sender(chatId, _client));
            var stateName = IOC.Container.Get<IAppSessionService>().Pupil.PupilState.State.ToString();
            var factory = IOC.Container.Get<IHandler<StateNameDto, State>>();
            var stateEntity = factory.Handle(new StateNameDto() {StateInfo = stateName});
            stateEntity.Execute(e.Update);
//            var username = e.Update.Message?.From?.Username ?? e.Update.CallbackQuery.From.Username;
        }
    }
}