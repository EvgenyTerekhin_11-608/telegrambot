namespace Decide.TelegramBot
{
    public class TelegramBotSettings
    {
        public string TelegramToken { get; set; }
        public ProxyInfo ProxyInfo { get; set; }
    }

    public class ProxyInfo
    {
        public string Address { get; set; }
        public Credentials Credentials { get; set; }
    }

    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}