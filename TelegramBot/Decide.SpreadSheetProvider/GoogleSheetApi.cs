﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Decide.AppSettings;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;

namespace Decide.SpreadSheetProvider
{
    public interface IGoogleSheetApi
    {
        IEnumerable<IEnumerable<string>> GetListInfo(string listName);
        IEnumerable<IEnumerable<string>> GetAreaInfo(string listname, string left, string rigth);
        IEnumerable<string> GetAllListNames();
    }

    
    public class MockGoogleSheetApi : IGoogleSheetApi
    {
        

        public IEnumerable<IEnumerable<string>> GetListInfo(string listName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IEnumerable<string>> GetAreaInfo(string listname, string left, string rigth)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetAllListNames()
        {
            throw new NotImplementedException();
        }
    }

    public class GoogleSheetApi : IGoogleSheetApi
    {
        #region creds

        private string SpreadsheetId { get; set; } = "1jUTlFM6o-LHOBHs47zraNo3gYbP1-EjJ99AwsHlPbCY";
        private UserCredential credential;
        const string credPath = "token.json";
        static string[] Scopes = {SheetsService.Scope.SpreadsheetsReadonly};
        static string ApplicationName = "Google Sheets API .NET Quickstart";

        #endregion

        private SheetsService SheetService { get; set; }


        public GoogleSheetApi()
        {
            #region connection

            var appsettings = AppSettingsProvider.GetSetting<SpreadSheetProviderSetting>("Decide.SpreadSheetProvider");

            SpreadsheetId = appsettings.spreadSheetApiToken;
            using (var stream =
                new FileStream(appsettings.credentialPath, FileMode.Open, FileAccess.Read))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            SheetService = new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName
            });

            #endregion
        }


        public IEnumerable<IEnumerable<string>> GetListInfo(string listName)
        {
            var range = $"{listName}";
            var request =
                SheetService.Spreadsheets.Values.Get(SpreadsheetId, range).Execute();
            var result = request.Values.Select(x => x.Select(xx =>
            {
                var str = xx.ToString();
                return str == string.Empty
                    ? null
                    : str;
            }));
            return result;
        }


        public IEnumerable<IEnumerable<string>> GetAreaInfo(string listname, string left, string rigth)
        {
            var range = $"{listname}!{left}:{rigth}";
            var request =
                SheetService.Spreadsheets.Values.Get(SpreadsheetId, range).Execute();
            var result = request.Values.Select(x => x.Select(xx => xx.ToString()));
            return result;
        }

        public IEnumerable<string> GetAllListNames()
        {
            var listNames = SheetService.Spreadsheets.Get(SpreadsheetId).Execute()
                .Sheets.Select(x => x.Properties.Title)
                .ToList();
            return listNames;
        }
    }
}