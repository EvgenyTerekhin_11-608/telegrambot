namespace Decide.SpreadSheetProvider
{
    public class SpreadSheetProviderSetting
    {
        public string spreadSheetApiToken { get; set; }
        public string credentialPath { get; set; }
    }
}