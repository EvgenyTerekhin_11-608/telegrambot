using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TestFSharp
{
    public static class TypeExtentions
    {
        public static MethodInfo GetMethodInfo(this Type source, string name, params Type[] parameters)
        {
            var result = source.GetMethod(name,
                BindingFlags.Static | BindingFlags.Public,
                null,
                CallingConventions.Any,
                parameters, null);

            return result;
        }

        public static IEnumerable<MethodInfo> GetMethodsInfo(this Type type, Func<MethodInfo, bool> predicate)
        {
            var result = type.GetMethods().Where(predicate);
            return result;
        }
        
        
    }
}