using System;

namespace TestFSharp
{
    public static class DateTimeExtentions
    {
        private static DateTime now => DateTime.Now;

        public static int GetRemainigMilliseconds(this DateTime dateTime)
        {
            if (!FutureDateTime(dateTime))
                throw new Exception("dateTime уже прошло");
            var result = dateTime.Subtract(now);
            return result.Milliseconds;
        }

        public static bool FutureDateTime(this DateTime dateTime) => dateTime > now;
    }
}