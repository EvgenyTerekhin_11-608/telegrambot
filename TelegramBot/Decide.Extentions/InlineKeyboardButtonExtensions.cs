using System.Collections.Generic;
using System.Linq;
using Telegram.Bot.Types.ReplyMarkups;

namespace TestFSharp
{
    public static class InlineKeyboardButtonExtensions
    {
        public static List<List<T>> ToTail<T>(this List<T> list, int tail)
        {
            var result = new List<List<T>>();
            while (list.Count >= tail)
            {
                result.AddRange(list.Take(tail).ToList().OneToList());
                list.Skip(tail);
            }

            result.AddRange(list.OneToList());
            return result;
        }

        public static InlineKeyboardMarkup GetInlineKeyboardMarkup(this IEnumerable<string> values, int tail)
        {
            var result =
                new InlineKeyboardMarkup(values.Select(InlineKeyboardButton.WithCallbackData)
                    .ToList().ToTail(tail));
            return result;
        }
    }
}