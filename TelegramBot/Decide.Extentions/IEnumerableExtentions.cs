using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using TestFSharp.Attributes;

namespace TestFSharp
{
    public enum ConcatType
    {
        [Concat('\0')] None,
        [Concat(' ')] Space,
        [Concat('\n')] Line
    }


    public static class IEnumerableExtentions
    {
        public static bool ParametersEquals<T>(this IEnumerable<T> left, IEnumerable<T> right)
        {
            return left.Join(right, x => x, y => y, (x, y) => x).Count() == left.Count();
        }

        public static string StringReduce(this IEnumerable<string> enumerable, ConcatType concatType = ConcatType.Line)
        {
            var concatSymbol = concatType.GetAttributeOfType<Concat>()._concatSymbol;
            var result = enumerable.Aggregate((a, c) =>
                $"{a}{concatSymbol}{c}");
            return result;
        }

        public static IEnumerable<T> SortByOtherSequence<T, T1>(this IEnumerable<T> enumerable,
            IEnumerable<T> enumerable1,
            Func<T, T1> comparasionPredicate = null) =>
            comparasionPredicate == null
                ? enumerable.Join(enumerable1, x => x, y => y, (x, y) => x)
                : enumerable.Join(enumerable1, comparasionPredicate, comparasionPredicate, (x, y) => x);

        public static IEnumerable<T2> SortByOtherSequence<T, T1, T2>(this IEnumerable<T> enumerable,
            IEnumerable<T> enumerable1,
            Func<T, T, T2> resultSelector,
            Func<T, T1> comparasionPredicate = null) =>
            comparasionPredicate == null
                ? enumerable.Join(enumerable1, x => x, y => y, resultSelector)
                : enumerable.Join(enumerable1, comparasionPredicate, comparasionPredicate, resultSelector);


        public static IEnumerable<T1> SortByOtherSequenceFrom<T, T1, T2>(this IEnumerable<T> enumerable,
            IEnumerable<T1> enumerable1, Func<T1, T> thisInnerOuterSelector, Func<T, T2> OuterPredicate = null)
        {
            var selectedEnumerable = enumerable1
                .Zip(enumerable1.Select(thisInnerOuterSelector), (source, destination) => (source, destination));

            var res = OuterPredicate == null
                ? enumerable.Join(selectedEnumerable, x => x, x => x.Item2, (x, y) => y.Item1)
                : enumerable.Join(selectedEnumerable, OuterPredicate, x => OuterPredicate(x.Item2),
                    (x, y) => y.Item1);

            return res;
        }

        public static IEnumerable<T1> EntityToEnumerable<T, T1>(this IEnumerable<T> enumerable, Func<T, T1> mapFn1,
            Func<T, T1> mapFn2)
        {
            var e1 = enumerable.Select(mapFn1);
            var e2 = enumerable.Select(mapFn2);
            var result = e1.Concat(e2);
            return result;
        }

        public static List<T> OneToList<T>(this T entity) => new List<T> {entity};


        public static List<T> IfLessToAdd<T>(this IEnumerable<T> enumerable, int count, Func<int, T> replaceFn = null)
        {
            var diff = count - enumerable.ToList().Count;
            var en = Enumerable.Range(0, diff).Select(x => replaceFn == null ? default(T) : replaceFn(x));
            return enumerable.Concat(en).ToList();
        }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
            {
                action(item);
            }
        }

        public static T ConcatThrough<T>(this IEnumerable<T> enumerable, Func<T, T, T, T> reduce,
            T through)
        {
            var result = enumerable.Aggregate((a, c) => reduce(a, through, c));
            return result;
        }
    }
}