using System;
using System.Runtime.InteropServices.ComTypes;

namespace TestFSharp
{
    public static class TimeExtentions
    {
        public static TimeSpan ByDateTime(this DateTime dateTime)
        {
            var current = DateTime.Now;

            var day = dateTime.Day;
            var hour = dateTime.Hour;
            var minutes = dateTime.Minute;
            var seconds = dateTime.Second;

            var c_day = current.Day;
            var c_hour = current.Hour;
            var c_minutes = current.Minute;
            var c_seconds = current.Second;

            return new TimeSpan(day - c_day, hour - c_hour, minutes - c_minutes, seconds - c_seconds);
        }
    }
}