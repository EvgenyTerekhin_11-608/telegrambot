using System;

namespace TestFSharp.Attributes
{
    public class Concat : Attribute
    {
        public readonly char _concatSymbol;

        public Concat(char concatSymbol)
        {
            _concatSymbol = concatSymbol;
        }
    }
}