using System;
using System.Linq;
using System.Reflection;

namespace TestFSharp
{
    public static class EnumExtentions
    {
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString()).First();
            var attribute = memInfo.GetCustomAttribute<T>();
            return attribute;
        }
    }
}