using System;
using Telegram.Bot.Types;

namespace TestFSharp
{
    public static class UpdateExtensions
    {
        public static int GetChatId(this Update update)
        {
            var result = update.Message?.Chat?.Id ?? update.CallbackQuery?.Message?.Chat?.Id;
            if (!result.HasValue)
                throw new Exception("ChatId не удалось получить");
            return (int) result.Value;
        }
    }
}