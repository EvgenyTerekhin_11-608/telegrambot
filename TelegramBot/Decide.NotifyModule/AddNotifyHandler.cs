using Decide.EventsAnalyzer;
using Force;

namespace NotifyModule
{
    public class AddNotifyHandler : IHandler<Event>
    {
        public void Handle(Event input)
        {
            EventAnalyzer.Add(input);
        }
    }
}