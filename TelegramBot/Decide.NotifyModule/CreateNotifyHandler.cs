using Decide.EventsAnalyzer;
using Force;
using TestFSharp;
using TestFSharp.Domain;

namespace NotifyModule
{
    public class CreateNotifyHandler : IHandler<Notification, Event>
    {
        private readonly IHandler<AggregateNotificationDto> _handler;

        public CreateNotifyHandler(IHandler<AggregateNotificationDto> handler)
        {
            _handler = handler;
        }

        public Event Handle(Notification input)
        {
            var genericHandlerTypeName = input.TInHandlerGeneric;
            var HandleMethodParameterValue = input.MethodInfo.ValueArgumentsType;
            var aggregateNotificationInfo =
                new AggregateNotificationDto(genericHandlerTypeName, HandleMethodParameterValue);
            var dateStart = input.ActionDate;
            return new Event(dateStart, () => _handler.Handle(aggregateNotificationInfo),input.Id);
        }
    }
}