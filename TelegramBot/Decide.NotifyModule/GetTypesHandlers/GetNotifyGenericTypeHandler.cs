using System;
using System.Collections.Generic;
using System.Linq;
using Force;
using TestFSharp.Handlers;

namespace TestFSharp
{
    public class GetNotifyGenericTypeHandler : Force.IHandler<string, GenericNotificationType>
    {
        private readonly IHandler<Func<Type, bool>, IEnumerable<Type>> _handler;

        public GetNotifyGenericTypeHandler(IHandler<Func<Type, bool>, IEnumerable<Type>> handler)
        {
            _handler = handler;
        }

        public GenericNotificationType Handle(string input)
        {
            var result =  _handler.Handle(predicate(input)).Single();
            return new GenericNotificationType(result);
        }

        private Func<string, Func<Type, bool>> predicate = x => y =>
            y.IsClass && typeof(INotificationEntity).IsAssignableFrom(y) &&
            y.Name == x;
    }
}