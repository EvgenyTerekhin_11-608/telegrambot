using System;

namespace TestFSharp
{
    public class GenericNotificationType
    {
        public Type _GenericType { get; set; }

        public GenericNotificationType(Type genericType)
        {
            _GenericType = genericType;
        }
    }
}