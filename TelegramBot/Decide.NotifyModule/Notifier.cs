
using Force;
using TestFSharp.Handlers;

namespace TestFSharp
{
    public class NotificateHandler : IHandler<AggregateNotificationDto>
    {
        private readonly IHandler<GetNotificationHandlerInput> _doHandler;
        private readonly Force.IHandler<string, GenericNotificationType> _getMethodHandler;

        public NotificateHandler(
            IHandler<GetNotificationHandlerInput> doHandler, Force.IHandler<string, GenericNotificationType> getMethodHandler)
        {
            _doHandler = doHandler;
            _getMethodHandler = getMethodHandler;
        }

        public void Handle(AggregateNotificationDto dto)
        {
            var types = _getMethodHandler.Handle(dto.GenericName);
            _doHandler.Handle(new GetNotificationHandlerInput(types._GenericType, dto.GenericTypeValue));
        }
    }
}