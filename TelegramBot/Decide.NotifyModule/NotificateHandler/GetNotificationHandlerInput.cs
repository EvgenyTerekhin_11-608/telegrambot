using System;
using Newtonsoft.Json.Linq;
using TestFSharp.Domain;
using TestFSharp.Handlers;

namespace TestFSharp
{
    public class GetNotificationHandlerInput
    {
        public Type GenericType { get; set; }
        public string GenericTypeValue { get; set; }

        public GetNotificationHandlerInput(Type genericType,  string genericTypeValue)
        {
            GenericType = genericType;
            GenericTypeValue = genericTypeValue;
        }
    }
}