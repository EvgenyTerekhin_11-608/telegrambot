using Newtonsoft.Json.Linq;

namespace TestFSharp
{
    public class AggregateNotificationDto
    {
        public string GenericName { get; set; }
        public string GenericTypeValue { get; set; }

        public AggregateNotificationDto(string genericName, string genericTypeValue)
        {
            GenericName = genericName;
            GenericTypeValue = genericTypeValue;
        }
    }
}