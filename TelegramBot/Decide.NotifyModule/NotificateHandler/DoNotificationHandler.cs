using System.Linq;
using System.Reflection;
using DIContainer;
using Force;
using Newtonsoft.Json;
using TestFSharp.Handlers;

namespace TestFSharp
{
    public class DoNotificationHandler : Force.IHandler<GetNotificationHandlerInput>
    {
        public void Handle(GetNotificationHandlerInput input)
        {
            var deserializeMethod = GetDeserializeMethod();

            var deserializeMethodWithGeneric = deserializeMethod.MakeGenericMethod(input.GenericType);

            var value = (dynamic) deserializeMethodWithGeneric.Invoke(this,
                new[] {input.GenericTypeValue});

            var entity =
                IOC.Container.Get(
                    typeof(INotificationHandler<>).MakeGenericType(input.GenericType));

            ((dynamic) entity).Handle((dynamic) value);
        }

        private MethodInfo GetDeserializeMethod() => typeof(JsonConvert).GetMethodsInfo(x =>
            x.Name == "DeserializeObject" && x.IsGenericMethod && x.GetParameters().Length == 1).Single();
    }
}