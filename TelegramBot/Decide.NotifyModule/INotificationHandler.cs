namespace TestFSharp.Handlers
{
    public interface INotificationHandler<TIn> where TIn:INotificationEntity
    {
        void Handle(TIn input);
    }

    public interface INotificationEntity
    {
    }

    public interface INotificationHandler
    {
        void Handle();
    }
}