﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using BotOnStateMachine;
using BotOnStateMachine.Commands;
using Decide.BotStructure;
using Decide.CodeGenerator;
using Decide.SpreadSheetProvider;
using Decide.StateStructureBuild;
using Decide.TelegramBot;
using Decide.TelegramBot.BaseClasses;
using DIContainer;
using Force;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using TelegramStateMachine.Infrastructure.Validators;
using TestFSharp;


namespace Interoperability.CSharp
{
    public static class Program
    {
        private static void Main()
        {
            var context = new BotDbContext();
            var appSessionService = new AppSessionService(context, 1);
            IOC.Container.Register<DbContext, BotDbContext>(new BotDbContext());
            IOC.Container.Register<IDataSet, DataSet>();
            IOC.Container.Register<IHandler<WriteFileInfo>, TextWriterHandler>();
            IOC.Container.Register<IHandler<ReadFileInfo, string>, ReadWriterHandler>();
            IOC.Container.Register<IHandler<WriteFileInfo>, SaveTextWriterHandle>(new SaveTextWriterHandle(
                new ReadWriterHandler(), new TextWriterHandler()));
            IOC.Container.Register<ISender, Sender>(new Sender(1, new TelegramBotClient("213", new WebProxy("2312"))));
            IOC.Container.Register<IAppSessionService, AppSessionService>(appSessionService);
            var s = new StructRegister(new RegisterInfoFactory(), new RegisterByBaseType(new GetTypeHandler()));
            s.Registration();
            IOC.Container.Register<IGoogleSheetApi, GoogleSheetApi>(new GoogleSheetApi());
//            IOC.Container.Register<IUploadModelInSpreadSheet, UploadModelInSpreadSheet>();
//            IOC.Container.Register<IUploadModelInSpreadSheet, WriteUploadModelInSpreadSheetProxy>
//            (new WriteUploadModelInSpreadSheetProxy(
//                new UploadModelInSpreadSheet(IOC.Container.Get<IGoogleSheetApi>()),
//                IOC.Container.Get<IHandler<WriteFileInfo>>()));
            IOC.Container.Register<IUploadModelInSpreadSheet, ReadUploadModelInSpreadSheetProxy>();
            IOC.Container.Register<IHandler<Func<Type, bool>, IEnumerable<Type>>, GetTypeHandler>();
            IOC.Container.Register<IHandler<StateNameDto, State>, StateFactory>();
            var factory = IOC.Container.Get<IHandler<StateNameDto, State>>();
            new TelegramBotManager();
        }
    }
}