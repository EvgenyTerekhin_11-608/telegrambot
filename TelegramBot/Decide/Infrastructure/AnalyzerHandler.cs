using System.Linq;
using System.Xml.Linq;
using TestFSharp.Domain;
using TestFSharp.Infrastructure;

namespace TestFSharp
{
    public class Ans
    {
        public Ans(int sCount, int fCount)
        {
            S_Count = sCount;
            F_Count = fCount;
        }

        public int S_Count { get; set; }
        public int F_Count { get; set; }
    }

    public class AnalyzerHandler
    {
        private readonly BotDbContext _context;
        private readonly int _examId;

        public AnalyzerHandler(BotDbContext context, int examId)
        {
            _context = context;
            _examId = examId;
        }

        public Ans Handle()
        {
            var s = _context.Exams.Where(x => x.Id == _examId).SelectMany(x => x.Answers);
            var succ = s.Select(x =>
                new
                {
                    task = x.Task,
                    Answer = x.AnswerString
                }).ToList();

            var ss = succ
                .Count(x => x.task.AnswerText.Contains(x.Answer));
            var result = new Ans(ss, succ.Count - ss);
            return result;
        }
    }
}