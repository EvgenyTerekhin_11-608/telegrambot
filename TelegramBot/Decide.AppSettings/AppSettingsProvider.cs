﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Decide.AppSettings
{
    public class AppSettingsProvider
    {
        //for tests
        private static string BasePathW = @"C:\Users\evgeniy\Documents\telegrambot\TelegramBot";
        private static string BasePathU = @"/home/evgeny/RiderProjects/TestFSharp/TelegramBot";

        public static T GetSetting<T>(string assemblyName)
        {
            var path = $@"{BasePathW
                }/{assemblyName}/appsettings.json";
            //
            var appsetting_text = File.ReadAllText(path);
            var result = JsonConvert.DeserializeObject<T>(appsetting_text);
            return result;
        }
    }
}