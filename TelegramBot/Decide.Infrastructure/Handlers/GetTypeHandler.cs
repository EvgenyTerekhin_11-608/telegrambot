using System;
using System.Collections.Generic;
using System.Linq;
using Force;

namespace TestFSharp
{
    public class GetTypeHandler : IHandler<Func<Type, bool>, IEnumerable<Type>>
    {
        
        public IEnumerable<Type> Handle(Func<Type, bool> predicate)
        {
            var type = Type(predicate);
            return type;
        }

        IEnumerable<Type> Type(Func<Type, bool> predicate) =>
            AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(x => !x.IsDynamic)
                .SelectMany(x => x.GetExportedTypes())
                .Where(predicate);
    }
}