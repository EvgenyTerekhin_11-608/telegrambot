using System;

namespace Decide.Infrastructure.Attributes
{
    public interface ISayError
    {
        string SayError();
    }

    public abstract class SayErrorAttribute : Attribute, ISayError
    {
        //currentValue,ComparisonValue => boolean
        private readonly object _currentValue;
        private readonly Func<object, object, bool> _func;

        //currentValue,ComparisonValue => void
        private readonly Func<object, object, string> _stringAction;
        private object ComparionsValue { get; set; }
        private object CurrentValue { get; set; }

        protected SayErrorAttribute(object comparisonValues, Func<object, object, bool> func,
            Func<object, object, string> stringAction)
        {
            ComparionsValue = comparisonValues;
            _func = func;
            _stringAction = stringAction;
        }

        public bool Match(object currentValue)
        {
            CurrentValue = currentValue;
            return _func(CurrentValue, ComparionsValue);
        }

        public string SayError()
        {
            return _stringAction(CurrentValue, ComparionsValue);
        }
    }
}