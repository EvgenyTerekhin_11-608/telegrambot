using System;

namespace Decide.Infrastructure.Attributes
{
    public class Max : SayErrorAttribute
    {
        public Max(object comparisonValue) : base(
            comparisonValue,
            (cur, com) => (int) cur <= (int) com,
            (cur, com) => $"Значение {cur} не должно быть больше {com}")
        {
        }
    }
}