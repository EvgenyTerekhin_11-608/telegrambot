using System;
using System.Runtime.CompilerServices;

namespace Decide.Infrastructure.Attributes
{
    public class Min : SayErrorAttribute
    {
        public Min(object currentValue) :
            base(currentValue,
                (cur, com) => (int) cur >= (int) com,
                (cur, com) => $"Значение {cur} не может быть меньше {com}")
        {
        }
    }
}