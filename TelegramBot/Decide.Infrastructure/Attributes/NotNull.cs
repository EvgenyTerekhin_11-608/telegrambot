using System;

namespace Decide.Infrastructure.Attributes
{
    public class NotNull : SayErrorAttribute
    {
        public NotNull() :
            base(null,
                (cur, com) => cur != null,
                (cur, com) => $"Значение не должно быть null")
        {
        }
    }
}