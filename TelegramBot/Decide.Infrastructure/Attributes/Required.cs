using System;

namespace Decide.Infrastructure.Attributes
{
    public class Required : SayErrorAttribute
    {
        public Required() :
            base(null,
                (cur, acc) =>
                {
                    if (cur == null)
                        return false;
                    var curType = cur.GetType();
                    if (curType.IsValueType)
                        return Activator.CreateInstance(curType) == cur;
                    return true;
                },
                (cur, acc) => "Должен быть Required")
        {
        }
    }
}