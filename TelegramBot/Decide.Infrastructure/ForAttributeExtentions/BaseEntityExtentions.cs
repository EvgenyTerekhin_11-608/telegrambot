using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Decide.Infrastructure.Attributes;

namespace TestFSharp
{
    internal static class BaseEntityExtentions
    {
        public class PropertyParsingValue
        {
            public string PropertyName { get; set; }
            public object PropertyValue { get; set; }
            public IEnumerable<SayErrorAttribute> Attributes { get; set; }

            public PropertyParsingValue(string propertyName, object propertyValue,
                IEnumerable<SayErrorAttribute> attributes)
            {
                PropertyName = propertyName;
                PropertyValue = propertyValue;
                Attributes = attributes;
            }
        }

        public static IEnumerable<PropertyParsingValue> GetAttributes<T>(this T obj)
            where T : class
        {
            var dic = obj.GetType().GetProperties()
                .Select(x =>
                    new PropertyParsingValue(x.Name, x.GetValue(obj),
                        CustomAttributeExtensions.GetCustomAttributes((MemberInfo) x).Cast<SayErrorAttribute>()));

            return dic;
        }

        public static Boolean ErrorExists(this IEnumerable<PropertyParsingValue> propertyParsingValues)
        {
            var result = propertyParsingValues.Any(x => x.Attributes.Any(xx => !xx.Match(x.PropertyValue)));
            return result;
        }

        public static string StringErrorArgregate(this IEnumerable<PropertyParsingValue> propertyParsingValues)
        {
            var result = propertyParsingValues.Select(x =>
                    new
                    {
                        propertyName = x.PropertyName,
                        errorMessage = x.Attributes.Aggregate(string.Empty, (acc, cur) =>
                            !cur.Match(x.PropertyValue)
                                ? acc + '\n' + cur.SayError()
                                : acc)
                    })
                .Where(x => x.errorMessage != string.Empty)
                .Select(x => $"{x.propertyName}{x.errorMessage}")
                .Aggregate((acc, cur) => $"{acc}\n\n{cur}");

            return result;
        }
    }
}