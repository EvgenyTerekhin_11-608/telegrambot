using System;
using System.Diagnostics.Contracts;
using System.Xml.Linq;

namespace TestFSharp
{
    public class BaseEntity
    {
        protected void CheckAttributes()
        {
            var attributes = this.GetAttributes();
            if (attributes.ErrorExists())
            {
                throw new Exception('\n' + attributes.StringErrorArgregate());
            }
        }
    }
}