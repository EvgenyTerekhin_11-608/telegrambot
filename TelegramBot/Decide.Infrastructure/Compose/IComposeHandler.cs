namespace TestFSharp
{
    public interface IComposeHandler<TIn>
    {
        TIn Handle(TIn input);
    }
}