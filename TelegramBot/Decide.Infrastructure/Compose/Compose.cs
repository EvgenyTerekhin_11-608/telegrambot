using System.Globalization;
using System.Linq;
using Force;

namespace TestFSharp
{
    public abstract class Compose<TIn> : IComposeHandler<TIn>
    {
        public abstract IHandler<TIn, TIn>[] functions { get; set; }

        public TIn Handle(TIn input)
        {
            var s = functions.Aggregate(input, (acc, cur) => cur.Handle(acc));
            return s;
        }
    }
    
  
}