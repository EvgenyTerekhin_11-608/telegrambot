using System;

namespace Decide.EventsAnalyzer
{
    public class Event : IEvent
    {
        public DateTime StartDate { get; set; }
        public Action Do { get; set; }
        
        public int NotificationId { get; set; }

        public Event(DateTime startDate, Action @do, int notificationId)
        {
            StartDate = startDate;
            Do = @do;
            NotificationId = notificationId;
        }
    }
}