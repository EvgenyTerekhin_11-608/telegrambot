using System;

namespace Decide.EventsAnalyzer
{
    public interface IEvent
    {
        DateTime StartDate { get; set; }
        Action Do { get; set; }
    }
}