﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TestFSharp;

namespace Decide.EventsAnalyzer
{
    public static class EventAnalyzer
    {
        static EventAnalyzer()
        {
            _events = new List<Event>();
            _current = (
                new Event(DateTime.Now.Add(new TimeSpan(0, 0, 30)),
                    () => { Console.WriteLine("Event Receiving"); },0),
                new CancellationTokenSource());
            Observable.Wait();
        }

        private static List<Event> _events;
        private static (Event, CancellationTokenSource) _current;

        public static void Add(Event @event)
        {
            _events.Add(@event);
            var isRollbackTask = EventsSorter.ChangeNeededNearestEvent(@event);
            if (!isRollbackTask) return;
            Cancel.CancelPreviousTask();
            ChangeCurrentEvent.ChangeIfNeeded();
            Observable.Wait();
        }

        static class Observable
        {
            public static void Wait()
            {
                if (_events.Any())
                    WaitNew();
            }

            private static async void WaitNew()
            {
                await Task.Delay(_current.Item1.StartDate.GetRemainigMilliseconds(), _current.Item2.Token)
                    .ContinueWith((x)
                        =>
                    {
                        //if Task.delay throw exception (cancellation token is cancel)
                        if (x.IsCanceled) return;
                        //
                        _current.Item1.Do();
                        RemoveEvent.Remove(_current.Item1.NotificationId);
                    });
            }
        }


        static class RemoveEvent
        {
            public static void Remove(int notificationId)
            {
                _events.Remove(_current.Item1);
                if (_current.Item1.NotificationId == notificationId)
                {
                    ChangeCurrentEvent.ChangeIfNeeded();
                    Observable.Wait();
                }

            }
        }

        static class EventsSorter
        {
            private static bool IsChangeNearestEvent(Event addedEvent) =>
                _current.Item1?.StartDate > addedEvent.StartDate;

            public static bool ChangeNeededNearestEvent(Event addedEvent)
            {
                _events = _events.OrderBy(x => x.StartDate).ToList();
                var need = IsChangeNearestEvent(addedEvent);
                return need;
            }
        }

        static class ChangeCurrentEvent
        {
            public static void ChangeIfNeeded()
            {
                if (_events.Any())
                    _current = (_events.First(), new CancellationTokenSource());
            }
        }

        static class Cancel
        {
            public static void CancelPreviousTask()
            {
                _current.Item2.Cancel();
            }
        }
    }
}