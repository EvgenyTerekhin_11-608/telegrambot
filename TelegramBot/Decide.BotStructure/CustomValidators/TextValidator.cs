using System;
using System.Linq;
using BotOnStateMachine.Commands;
using Telegram.Bot.Types;
using TelegramStateMachine.Infrastructure.Validators;

namespace TelegramStateMachine.CustomValidators
{
    public class TextValidator : PredicateValidator
    {
        public TextValidator() : base(x => x?.Message?.Text != null)
        {
        }
    }

    public class DigitButtonValidator : PredicateValidator
    {
        public DigitButtonValidator() : base(x =>
        {
            var data = x?.CallbackQuery?.Data;
            if (data == null)
                return false;
            var result = data.Aggregate(true, (a, c) => a && char.IsDigit(c));
            return result;
        })
        {
        }
    }

    public class DigitMessageValidator : PredicateValidator
    {
        public DigitMessageValidator(Func<Update, bool> predicate) : base(x =>
        {
            var data = x.Message?.Text;
            if (data == null)
                return false;
            var result = data.Aggregate(true, (a, c) => a && char.IsDigit(c));
            return result;
        })
        {
        }
    }

    public class ExamTimeValidator : IValidator
    {
        public bool isValid(Update update)
        {
            throw new System.NotImplementedException();
        }
    }
}