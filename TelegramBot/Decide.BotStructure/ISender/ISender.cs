using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BotOnStateMachine.Commands
{
    public interface ISender
    {
        Task Send(string message, IReplyMarkup buttons = null);
        Task Send(string message);
    }
}