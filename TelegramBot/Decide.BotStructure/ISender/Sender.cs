using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace BotOnStateMachine.Commands
{
    public class Sender : ISender
    {
        private readonly long ChatId;
        private readonly TelegramBotClient Client;

        public Sender(long chatId, TelegramBotClient client)
        {
            ChatId = chatId;
            Client = client;
        }

        public async Task Send(string message)
        {
            await Client.SendTextMessageAsync(ChatId, message);
        }


        public async Task Send(string message, IReplyMarkup buttons)
        {
            await Client.SendTextMessageAsync(ChatId, message, replyMarkup: buttons);
        }
    }
}