using TestFSharp;
using TestFSharp.Domain;

namespace Decide.BotStructure
{
    public interface IAppSessionService
    {
        Pupil Pupil { get; }
    }

    public class AppSessionService : IAppSessionService
    {
        private readonly BotDbContext _context;
        private readonly int _chatId;
        private Pupil _pupil { get; set; }

        public Pupil Pupil => _pupil ?? (_pupil = _context.Pupils.Find(_chatId));

        public AppSessionService(BotDbContext context, int chatId)
        {
            _context = context;
            _chatId = chatId;
        }
    }
}