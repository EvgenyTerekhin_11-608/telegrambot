using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using BotOnStateMachine.Commands;
using DIContainer;
using Force;
using TestFSharp;

namespace Decide.BotStructure
{
    public class RegisterByBaseType : IHandler<List<RegisterInfo>>
    {
        private readonly IHandler<Func<Type, bool>, IEnumerable<Type>> _getType;

        public RegisterByBaseType(IHandler<Func<Type, bool>, IEnumerable<Type>> getType)
        {
            _getType = getType;
        }

        public void Handle(List<RegisterInfo> registerInfos)
        {
            var services = registerInfos.SelectMany(info => _getType.Handle(info.filterPredicate)).ToList();
            services.ForEach(x => IOC.Container.Register(x));
        }
    }
}