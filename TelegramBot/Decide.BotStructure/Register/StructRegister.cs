using System;
using System.Collections.Generic;
using System.Linq;
using Force;

namespace Decide.BotStructure
{
    public class StructRegister
    {
        private readonly IRegisterInfoFactory _registerInfoFactory;
        private readonly IHandler<List<RegisterInfo>> _registerByBaseTypeHandler;

        public StructRegister(IRegisterInfoFactory registerInfoFactory,
            IHandler<List<RegisterInfo>> registerByBaseTypeHandler)
        {
            _registerInfoFactory = registerInfoFactory;
            _registerByBaseTypeHandler = registerByBaseTypeHandler;
        }

        public void Registration()
        {
            var results =
                _registerInfoFactory.GetType().GetMethods()
                    .Where(x => x.IsPublic && x.IsFinal)
                    .Select(x => x.Invoke(_registerInfoFactory, parameters: new object[] { }))
                    .Cast<RegisterInfo>()
                    .ToList();

            _registerByBaseTypeHandler.Handle(results);
        }
    }
}