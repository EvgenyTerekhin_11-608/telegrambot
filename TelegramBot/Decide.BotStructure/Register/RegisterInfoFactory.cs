using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using BotOnStateMachine.Commands;
using TelegramStateMachine.Infrastructure.Validators;

namespace Decide.BotStructure
{
    public interface IRegisterInfoFactory
    {
        RegisterInfo UseMeRegisterInfoCreate();
        RegisterInfo CommandRegisterInfoCreate();
        RegisterInfo ValidatorsRegisterInfoCreate();
    }

    public class RegisterInfoFactory : IRegisterInfoFactory
    {
        private Func<Type, bool> BaseRegisterInfoCreateFunc(Type t)
        {
            return x => !x.IsAbstract && t.IsAssignableFrom(x);
        }

        public RegisterInfo UseMeRegisterInfoCreate()
        {
            return new RegisterInfo(BaseRegisterInfoCreateFunc(typeof(UseMe)));
        }

        public RegisterInfo CommandRegisterInfoCreate()
        {
            return new RegisterInfo(BaseRegisterInfoCreateFunc(typeof(Command)));
        }

        public RegisterInfo ValidatorsRegisterInfoCreate()
        {
            return new RegisterInfo(x => BaseRegisterInfoCreateFunc(typeof(IValidator))(x) &&
                                         x.GetConstructors().All(xx => xx.GetParameters().Length == 0));
        }
    }
}