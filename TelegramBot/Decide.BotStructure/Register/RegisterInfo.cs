using System;

namespace Decide.BotStructure
{
    public class RegisterInfo
    {
        public Func<Type, bool> filterPredicate { get; set; }

        public RegisterInfo(Func<Type, bool> filterPredicate)
        {
            this.filterPredicate = filterPredicate;
        }
    }
}