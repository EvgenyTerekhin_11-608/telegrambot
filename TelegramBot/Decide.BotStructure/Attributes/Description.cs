using System;

namespace Decide.BotStructure.Attributes
{
    public class Description:Attribute
    {
        private readonly string _description;

        public Description(string description)
        {
            _description = description;
        }
    }
}