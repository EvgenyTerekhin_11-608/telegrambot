using Decide.TelegramBot.BaseClasses;
using Telegram.Bot.Types;

namespace BotOnStateMachine.Commands
{
    public class DefaultUseMe:UseMe
    {
        public DefaultUseMe(IDataSet dataSet) : base(dataSet)
        {
        }

        public override bool Check(Update update)
        {
            return true;
        }
    }
}