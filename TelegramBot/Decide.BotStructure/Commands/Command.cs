using Decide.BotStructure.Attributes;
using Decide.TelegramBot.BaseClasses;
using DIContainer;
using Telegram.Bot.Types;

namespace BotOnStateMachine.Commands
{
    public class Command
    {
        protected readonly IDataSet _dataSet;
        protected readonly ISender _sender;

        public Command(IDataSet dataSet, ISender sender)
        {
            _dataSet = dataSet;
            _sender = sender;
        }

        [Description("description")]
        public virtual void Execute(Update update)
        {
            throw new System.NotImplementedException();
        }
    }
}