using Decide.BotStructure;
using Microsoft.EntityFrameworkCore;
using Telegram_Bot_Machine.Models;
using TestFSharp;

namespace BotOnStateMachine.Commands
{
    public class TransactionManager
    {
        private readonly StateType StateType;
        private readonly IAppSessionService _appSessionService;
        private readonly BotDbContext _context;

        public TransactionManager(StateType stateType, IAppSessionService appSessionService, BotDbContext context)
        {
            StateType = stateType;
            _appSessionService = appSessionService;
            _context = context;
        }

        public void ChangeState()
        {
            _appSessionService.Pupil.ChangeState(StateType, _context);
        }
    }
}