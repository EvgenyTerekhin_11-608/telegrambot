using System.Collections.Generic;
using Telegram_Bot_Machine.Models;

namespace Decide.BotStructure
{
    public class StateTypeInfo
    {
        public StateTypeInfo(IEnumerable<string> useMes, IEnumerable<string> commands, IEnumerable<string> validators,
            IEnumerable<string> contains)
        {
            UseMes = useMes;
            Commands = commands;
            Validators = validators;
            Contains = contains;
        }

        public IEnumerable<string> UseMes { get; set; }
        public IEnumerable<string> Commands { get; set; }
        public IEnumerable<string> Validators { get; set; }
        public IEnumerable<string> Contains { get; set; }
    }

    public class NavigationDictionary
    {
        public static Dictionary<StateType, StateTypeInfo> _dic = new Dictionary<StateType, StateTypeInfo>
        {
            {
                StateType.UnAuth,
                new StateTypeInfo(
                    new List<string> {"1"},
                    new List<string> {"2"},
                    new List<string> {"3"},
                    new List<string> {"4"})
            },
            {
                StateType.UnAuth,
                new StateTypeInfo(
                    new List<string> {"1"},
                    new List<string> {"2"},
                    new List<string> {"3"},
                    new List<string> {"4"})
            }
        };
    }
}