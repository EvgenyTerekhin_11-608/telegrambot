using System;
using System.Linq;
using TestFSharp;

namespace Decide.TelegramBot.BaseClasses
{
    public interface IDataSet
    {
        IQueryable<T> Get<T>() where T : class;

        void Add<T>(T item) where T : class;

        void Remove<T>(T item) where T : class;

        void Update<T>(Func<T, bool> func, T value) where T : class;
    }

    public class DataSet : IDataSet
    {
        private readonly BotDbContext _context;

        public DataSet(BotDbContext context)
        {
            _context = context;
        }

        public IQueryable<T> Get<T>() where T : class
        {
            return _context.Set<T>();
        }

        public void Add<T>(T item) where T : class
        {
            _context.Add(item);
        }

        public void Remove<T>(T item) where T : class
        {
            _context.Remove(item);
        }

        public void Update<T>(Func<T, bool> func, T value)  where T : class
        {
            var item = _context.Set<T>().Single(func);
            if(item == null)
               throw new Exception("Такого элемента не сущетсвует");
            item = value;
            _context.SaveChanges();
        }
        
    }
}