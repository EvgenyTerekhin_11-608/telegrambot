using System;
using Decide.TelegramBot.BaseClasses;
using Telegram.Bot.Types;

namespace BotOnStateMachine.Commands
{
    public abstract class UseMe
    {
        protected readonly IDataSet DataSet;

        protected UseMe(IDataSet dataSet)
        {
            DataSet = dataSet;
        }

        public abstract bool Check(Update update);
    }

    public class UseMeImp : UseMe
    {
        public UseMeImp(IDataSet dataSet) : base(dataSet)
        {
        }

        public override bool Check(Update update)
        {
            throw new NotImplementedException();
        }
    }
}