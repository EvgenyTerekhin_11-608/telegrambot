using System;
using System.IO;
using System.Runtime.InteropServices;
using Force;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;

namespace Decide.CodeGenerator
{
    public class TextWriterHandler : IHandler<WriteFileInfo>
    {
        public void Handle(WriteFileInfo input)
        {
            File.WriteAllText(input.Path, input.Text);
        }
    }

    public class ReadWriterHandler : IHandler<ReadFileInfo, string>
    {
        public string Handle(ReadFileInfo input)
        {
            if (!File.Exists(input.Path))
                throw new Exception($"Файла {input.Path} не существует");
            var result = File.ReadAllText(input.Path);
            return result;
        }
    }

    public class SaveTextWriterHandle : IHandler<WriteFileInfo>
    {
        private readonly ReadWriterHandler _readWriterHandler;
        private readonly TextWriterHandler _textWriterHandler;

        public SaveTextWriterHandle(ReadWriterHandler readWriterHandler, TextWriterHandler textWriterHandler)
        {
            _readWriterHandler = readWriterHandler;
            _textWriterHandler = textWriterHandler;
        }

        public void Handle(WriteFileInfo input)
        {
            if (!File.Exists(input.Path))
            {
                _textWriterHandler.Handle(input);
                return;
            }

            var text = _readWriterHandler.Handle(new ReadFileInfo(input.Path));
            if (text != input.Text)
            {
                Console.WriteLine($"Файл {input.Path} перезаписан не будет");
                return;
            }

            _textWriterHandler.Handle(input);
        }
    }
}