namespace Decide.CodeGenerator
{
    public class WriteFileInfo : FileInfo
    {
        public string Text { get; set; }

        public WriteFileInfo(string path, string text) : base(path)
        {
            Text = text;
        }
    }

    public class ReadFileInfo : FileInfo
    {
        public ReadFileInfo(string path) : base(path)
        {
        }
    }

    public class FileInfo
    {
        public string Path { get; set; }

        public FileInfo(string path)
        {
            Path = path;
        }
    }
}