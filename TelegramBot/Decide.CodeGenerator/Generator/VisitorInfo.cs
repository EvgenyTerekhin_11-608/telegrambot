using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace Tests
{
    public class VisitorInfo
    {
        public List<ReplaceInfo> replaces { get; set; }
        public SyntaxNode node { get; set; }

        public Func<object, ReplaceInfo> Selector => tok => replaces.SingleOrDefault(x =>
            x.choosePredicate(tok.GetType()) && x.replacePredicate(tok.ToString()));


        public VisitorInfo(List<ReplaceInfo> replaces, SyntaxNode node)
        {
            this.replaces = replaces;
            this.node = node;
        }
    }
}