using System.Collections.Generic;
using System.IO;
using BotOnStateMachine.Commands;
using Decide.CodeGenerator;
using Decide.TelegramBot.UseMes;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Telegram_Bot_Machine.Models;
using TestFSharp;

namespace Tests
{
    public class VisitorInfoFactory : IVisitorAndFileInfoFactory
    {
        public VisitorAndFileInfo UseMeReplaceInfoCreate(Dictionary<string, string> replaceInsertDictionary)
        {
//            var path = "../../../../Decide.BotStructure/UseMes";
            var path = "../Decide.BotStructure/UseMes";
            var classImplName = nameof(UseMeImpl);
            var insertString = replaceInsertDictionary[classImplName];
            var syntaxCode = CSharpSyntaxTree.ParseText(File.ReadAllText($"{path}/UseMeImpl.cs")).GetRoot();
            var list = new ReplaceInfo(insertString, x => x == classImplName,
                    x => x.Name == nameof(SyntaxToken))
                .OneToList();
            return new VisitorAndFileInfo(new VisitorInfo(list, syntaxCode),
                new TextToFileInfo($"{path}/{insertString}.cs"));
        }

        public VisitorAndFileInfo CommandReplaceInfoCreate(Dictionary<string, string> replaceInsertDictionary)
        {
//            var path = "../../../../Decide.BotStructure/Commands";
          var path = "../Decide.BotStructure/Commands";
            var attributeArgumentName = "\"description\"";
            var classImplName = nameof(CommandImpl);
            var insertString = replaceInsertDictionary[classImplName];
            var insertAtributeName = replaceInsertDictionary[attributeArgumentName];

            var syntaxCode = CSharpSyntaxTree.ParseText(File.ReadAllText($"{path}/{classImplName}.cs")).GetRoot();
            var list = new List<ReplaceInfo>
            {
                new ReplaceInfo(insertString, x => x == classImplName, x => x.Name == nameof(SyntaxToken)),
                new ReplaceInfo(insertAtributeName, x => x == attributeArgumentName,
                    x => x.Name == nameof(AttributeArgumentSyntax))
            };
            return new VisitorAndFileInfo(new VisitorInfo(list, syntaxCode),
                new TextToFileInfo($"{path}/{insertString}.cs"));
        }

        public VisitorAndFileInfo StateReplaceInfoCreate(Dictionary<string, string> dictionary)
        {
//            var path = "../../../../Decide.Domain/Domain/StateType.cs";
            var path = "../Decide.Domain/Domain/StateType.cs";
            var insertString = dictionary[nameof(StateType)];
            var syntaxCode = CSharpSyntaxTree.ParseText(File.ReadAllText($"{path}")).GetRoot();
            var list = new List<ReplaceInfo>
            {
                new ReplaceInfo(insertString, x => x.Contains("StateType"),
                    x => x.Name == nameof(EnumDeclarationSyntax)),
            };
            return new VisitorAndFileInfo(new VisitorInfo(list, syntaxCode), new TextToFileInfo(path));
        }

//C:\Users\evgeniy\Documents\telegrambot\TelegramBot\Decide.Domain\Domain\StateType.cs
    }
}