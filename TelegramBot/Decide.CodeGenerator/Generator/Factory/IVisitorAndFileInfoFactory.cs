using System.Collections.Generic;
using Decide.CodeGenerator;

namespace Tests
{
    public interface IVisitorAndFileInfoFactory
    {
        VisitorAndFileInfo UseMeReplaceInfoCreate(Dictionary<string, string> _replaceInsertDictionary);

        VisitorAndFileInfo CommandReplaceInfoCreate(Dictionary<string, string> _replaceInsertDictionary);

        VisitorAndFileInfo StateReplaceInfoCreate(Dictionary<string, string> replaceInsertDictionary);
    }
}