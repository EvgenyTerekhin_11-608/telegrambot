using Tests;

namespace Decide.CodeGenerator
{
    public class VisitorAndFileInfo
    {
        public VisitorInfo VisitorInfo { get; set; }
        public TextToFileInfo FileInfo { get; set; }

        public VisitorAndFileInfo(VisitorInfo visitorInfo, TextToFileInfo fileInfo)
        {
            VisitorInfo = visitorInfo;
            FileInfo = fileInfo;
        }
    }
}