using System;
using System.Collections.Generic;
using Force;
using Tests;

namespace Decide.CodeGenerator
{
    public class CreateClassHandler :IHandler<VisitorAndFileInfo>
    {
        private IHandler<VisitorInfo,string> ReWriter { get; set; }
        private IHandler<WriteFileInfo> ClassToFIleWriter { get; set; }

        public CreateClassHandler(IHandler<VisitorInfo,string> reWriter, IHandler<WriteFileInfo> classToFIleWriter)
        {
            ClassToFIleWriter = classToFIleWriter;
            ReWriter = reWriter;
        }

        public void Handle(VisitorAndFileInfo info)
        {
            var code = ReWriter.Handle(info.VisitorInfo);
            ClassToFIleWriter.Handle(new WriteFileInfo(info.FileInfo.PathToNewFile, code));
        }
    }
}