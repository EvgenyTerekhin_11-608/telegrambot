using System.Collections.Generic;
using System.Linq;
using Force;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using TestFSharp;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace Decide.CodeGenerator.Generator
{
    public class EnumValuesInfo
    {
        public string EnumName { get; set; }
        public List<string> EnumValues { get; set; }

        public EnumValuesInfo(string enumName, List<string> enumValues)
        {
            EnumName = enumName;
            EnumValues = enumValues;
        }
    }


    public class EnumGenerator : IHandler<EnumValuesInfo, SyntaxTree>
    {
        private SyntaxNodeOrToken[] GetEnumArg(List<string> args)
        {
            var result = args
                .Select(x => new SyntaxNodeOrToken[] {EnumMemberDeclaration(Identifier(x))})
                .Aggregate((acc, curr) =>
                    acc.Concat(new SyntaxNodeOrToken[] {Token(SyntaxKind.CommaToken)})
                        .Concat(curr)
                        .ToArray());
            return result;
        }

        public SyntaxTree Handle(EnumValuesInfo input)
        {
            var @enum = EnumDeclaration(input.EnumName)
                .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword)))
                .WithMembers(SeparatedList<EnumMemberDeclarationSyntax>(GetEnumArg(input.EnumValues)))
                .NormalizeWhitespace();
            var enumST = CSharpSyntaxTree.Create(@enum);
            return enumST;
        }
    }
}