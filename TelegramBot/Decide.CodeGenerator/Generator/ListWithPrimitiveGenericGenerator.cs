using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Force;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using TestFSharp;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace Decide.CodeGenerator.Generator
{
    public class ListGenerationInfo
    {
        public ListGenerationInfo(Type generic, List<string> values)
        {
            Generic = generic;
            Values = values;
        }

        public Type Generic { get; set; }
        public List<string> Values { get; set; }
    }


    public class TypeToSyntaxKind : IHandler<Type, SyntaxKind>
    {
        public SyntaxKind Handle(Type input)
        {
            var syntaxKind = CSharpSyntaxTree.ParseText(input.Name.ToLower())
                .GetRoot().DescendantNodes().OfType<PredefinedTypeSyntax>()
                .Single().DescendantTokens().First().Kind();

            return syntaxKind;
        }
    }

    // for string,int,double,DateTime,float
    public class ListWithPrimitiveGenericGenerator : IHandler<ListGenerationInfo, SyntaxTree>
    {
        private readonly IHandler<Type, SyntaxKind> _typeToSyntax;

        private List<SyntaxTrivia> GetListBaseItem(SyntaxToken token)
        {
            return Trivia(
                    SkippedTokensTrivia()
                        .WithTokens(
                            TokenList(token)))
                .OneToList();
        }

        public ListWithPrimitiveGenericGenerator(IHandler<Type, SyntaxKind> typeToSyntax)
        {
            _typeToSyntax = typeToSyntax;
        }

        public SyntaxTree Handle(ListGenerationInfo input)
        {
            var genericSyntaxKind = _typeToSyntax.Handle(input.Generic);
            var initialArray = new ArrayList()
            {
                Token(genericSyntaxKind),
                Identifier("List"),
                Token(SyntaxKind.LessThanToken),
                Token(SyntaxKind.StringKeyword),
                Token(SyntaxKind.GreaterThanToken),
                Token(SyntaxKind.OpenBraceToken),
            };


            var s = CompilationUnit()
                .WithEndOfFileToken(
                    Token(
                        TriviaList(
                            new[]
                            {
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(genericSyntaxKind)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Identifier("List")))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.LessThanToken)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.StringKeyword)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.GreaterThanToken)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.OpenBraceToken)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Literal("1")))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.CommaToken)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Literal("2")))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.CommaToken)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Literal("3")))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.CloseBraceToken)))),
                                Trivia(
                                    SkippedTokensTrivia()
                                        .WithTokens(
                                            TokenList(
                                                Token(SyntaxKind.SemicolonToken))))
                            }),
                        SyntaxKind.EndOfFileToken,
                        TriviaList()))
                .NormalizeWhitespace();
            return null;
        }
    }
}