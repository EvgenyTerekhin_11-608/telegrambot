﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using BotOnStateMachine.Commands;
using Decide.BotStructure.Attributes;
using Decide.TelegramBot.UseMes;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Decide.CodeGenerator
{
    public class TextToFileInfo
    {
        public string PathToNewFile { get; set; }

        public TextToFileInfo(string pathToNewFile)
        {
            PathToNewFile = pathToNewFile;
        }
    }
}