using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Force;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using TestFSharp;
using Tests;

namespace Decide.CodeGenerator
{
    public class ReplaceHandler :IHandler<VisitorInfo,string>
    {
        private readonly ReplaceVisitor ReplaceVisitor;

        public ReplaceHandler(ReplaceVisitor ReplaceVisitor)
        {
            this.ReplaceVisitor = ReplaceVisitor;
        }

        public string Handle(VisitorInfo info)
        {
            var result = ReplaceVisitor.Visit(info);
            return result.ToString();
        }
    }
    
}