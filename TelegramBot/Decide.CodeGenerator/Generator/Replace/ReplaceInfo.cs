using System;

namespace Tests
{
    public class ReplaceInfo
    {
        public string _insertStr { get; set; }
        public Func<string, bool> replacePredicate { get; set; }
        public Func<Type, bool> choosePredicate { get; set; }


        public ReplaceInfo(string insertStr, Func<string, bool> replacePredicate,Func<Type, bool> choosePredicate)
        {
            this.choosePredicate = choosePredicate;
            _insertStr = insertStr;
            this.replacePredicate = replacePredicate;
        }
    }
}