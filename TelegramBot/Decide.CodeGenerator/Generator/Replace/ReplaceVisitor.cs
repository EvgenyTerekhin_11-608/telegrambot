using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Decide.BotStructure.Attributes;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Tests
{
    public class ReplaceVisitor : CSharpSyntaxRewriter
    {
        private VisitorInfo _info { get; set; }

        public SyntaxNode Visit(VisitorInfo info)
        {
            _info = info;
            return base.Visit(_info.node);
        }

        public override SyntaxNode VisitEnumDeclaration(EnumDeclarationSyntax node)
        {
            var replaceInfo = _info?.Selector(node);
            if (replaceInfo == null) return base.VisitEnumDeclaration(node);
            var visitEnumDeclaration = CSharpSyntaxTree.ParseText(replaceInfo._insertStr)
                .GetRoot()
                .DescendantNodes()
                .OfType<EnumDeclarationSyntax>()
                .Single();
            return visitEnumDeclaration;
        }

        public override SyntaxToken VisitToken(SyntaxToken token)
        {
            var replaceInfo = _info?.Selector(token);
            if (replaceInfo == null) return base.VisitToken(token);

            return SyntaxFactory.Identifier(token.LeadingTrivia, replaceInfo._insertStr, token.TrailingTrivia);
        }

        public override SyntaxNode VisitAttributeArgument(AttributeArgumentSyntax node)
        {
            var replaceInfo = _info?.Selector(node);
            if (replaceInfo == null) return base.VisitAttributeArgument(node);

            return SyntaxFactory.AttributeArgument(
                SyntaxFactory.LiteralExpression(SyntaxKind.StringLiteralExpression,
                    SyntaxFactory.Literal(replaceInfo._insertStr)));
        }
    }
}