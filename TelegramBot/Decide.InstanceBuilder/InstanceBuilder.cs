using System;
using System.Collections.Generic;
using System.Linq;
using TestFSharp;

namespace Decide.InstanceBuilder
{
    public class InstanceBuilder
    {
        private readonly LambdaBuilder _lambdaBuilder;

        public InstanceBuilder(LambdaBuilder lambdaBuilder)
        {
            _lambdaBuilder = lambdaBuilder;
        }

        public object Build(Type t, IEnumerable<object> parameterValues)
        {
            var expressionctor = _lambdaBuilder.GetExpressionCtor(t);
            var compile = expressionctor.Compile();
            var parameterInfos = compile.Method.GetParameters().Select(x => x.ParameterType);

            var orderParameters =
                parameterInfos.SortByOtherSequenceFrom(parameterValues, x => x.GetType(),
                    x => x.GetHashCode());
            var result = compile.DynamicInvoke(orderParameters.ToArray());
            return result;
        }

        public T Build<T>(Type t, IEnumerable<object> parameterValues)
        {
            var result = Build(t, parameterValues);
            var cast = (T) result;
            return cast;
        }
    }
}