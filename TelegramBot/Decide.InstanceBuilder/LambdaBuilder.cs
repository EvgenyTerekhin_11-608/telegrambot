using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Reflection;
using System.Xml.Linq;
using TestFSharp;

namespace Decide.InstanceBuilder
{
    public class LambdaBuilder
    {
        public LambdaExpression GetExpressionCtor(Type t, IEnumerable<Type> constructorParameters)
        {
            return GetExpression(t, GetParameters(t).SortByOtherSequence(constructorParameters, x => x.GetHashCode()));
        }

        private LambdaExpression GetExpression(Type t, IEnumerable<Type> constructorParameters)
        {
            var expression_parameters = constructorParameters.Select(Expression.Parameter).ToList();
            var ctor = t.GetConstructor(BindingFlags.Instance | BindingFlags.Public,
                null,
                CallingConventions.HasThis,
                constructorParameters.ToArray(),
                new ParameterModifier[0]);
            if (ctor == null)
                throw new Exception("Конструтора не существует");
            var ctor_expression = Expression.New(ctor, expression_parameters);
            var lambda = Expression.Lambda(ctor_expression, expression_parameters);
            return lambda;
        }

        public LambdaExpression GetExpressionCtor(Type t)
        {
            var ctorParameters = GetParameters(t);
            if (ctorParameters == null)
                throw new Exception("Такого конструктора не существует");
            return GetExpression(t, ctorParameters);
        }

        private IEnumerable<Type> GetParameters(Type t)
        {
            
            var ctorParameters = t.GetConstructors()
                .OrderBy(x => x.GetParameters().Length)
                .FirstOrDefault()
                ?.GetParameters()
                .Select(x => x.ParameterType);
            return ctorParameters;
        }
    }
}