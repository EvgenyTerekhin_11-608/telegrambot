﻿module Example
    type Person = {
    id : int
    name : string
    friends : string seq
    }

    let flipFSharpInnerTuple (val1, (val2, val3)) =
        (val1, (val3, val2))

    type LogLevels =
        | Test1 of Map<string, LogLevels>
        | Test of Map<string, string>

    let getMap =
        let map = Map.empty.Add ("sosi",(Map.empty.Add ("213", "123")))
        map
    let _value = getMap
    
