using System;

namespace DIContainer.EntityDeliverManager
{
    public class EntityDeliverManager
    {
        private readonly UseValueServiceDecorator _decorator;

        public EntityDeliverManager(
            UseValueServiceDecorator decorator)
        {
            _decorator = decorator;
        }

        public TIn Get<TIn>()
        {
            var result = _decorator.Handle(typeof(TIn));
            var cast = (TIn) result;
            return cast;
        }

        public object Get(Type TIn)
        {
            var result = _decorator.Handle(TIn);
            return result;
        }
    }
}