using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DIContainer.EntityDeliverManager
{
    public interface IDeliverService
    {
        EntityInfo Get<TIn>();
        EntityInfo Get(Type t);
    }

    public class DeliverService : IDeliverService
    {
        private readonly Storage _storage;

        public DeliverService(Storage storage)
        {
            _storage = storage;
        }

        public EntityInfo Get<TIn>()
        {
            var result = _storage.GetAll().Single(x => x.Key == typeof(TIn) || x.Value.ImpType == typeof(TIn)).Value;
            return result;
        }

        public EntityInfo Get(Type t)
        {
            var result = _storage.GetAll().Single(x => x.Key == t).Value;
            return result;
        }
    }
}