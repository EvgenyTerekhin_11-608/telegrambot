using System;
using System.Collections.Generic;
using System.Linq;
using DIContainer.Cache;

namespace DIContainer
{
    public class RegisterManager
    {
        private readonly IBuildLambdaRegisterService BuildLambdaRegisterService;
        private readonly IUseValueRegisterService _useValueRegisterService;

        public RegisterManager(IBuildLambdaRegisterService buildLambdaRegisterService,
            IUseValueRegisterService useValueRegisterService)
        {
            BuildLambdaRegisterService = buildLambdaRegisterService;
            _useValueRegisterService = useValueRegisterService;
        }

        public void Register(Type TIn, Type TImp, LifeTime lifeTime)
        {
            BuildLambdaRegisterService.Handle(TIn, TImp, lifeTime);
        }

        public void Register(Type TIn, Type TImp, object value, LifeTime lifeTime)
        {
            _useValueRegisterService.Handle(TIn, TImp, value, lifeTime);
        }
    }

    public class BuildLambdaRegisterService : IBuildLambdaRegisterService
    {
        private readonly CacheBase<Type, EntityInfo> _storage;
        private readonly CacheBase<Type, object> _useValueStorage;


        public BuildLambdaRegisterService(CacheBase<Type, EntityInfo> storage, CacheBase<Type, object> useValueStorage)
        {
            _storage = storage;
            _useValueStorage = useValueStorage;
        }

        public void Handle(Type TIn, Type TImp, LifeTime lifeTime)
        {
            var list = new List<EntityInfo>();
            TImp.GetConstructors()
                .OrderBy(x => x.GetParameters().Length)
                .First().GetParameters().ToList()
                .ForEach(x =>
                {
                    var info = _storage.GetAll()
                        .FirstOrDefault(xx =>
                            xx.Value.ImpType == x.ParameterType || xx.Key == x.ParameterType).Value;

                    if (!_useValueStorage.GetAll().Any(xx => xx.Key.IsAssignableFrom(x.ParameterType)) && info == null)
                        throw new Exception(
                            $"Сервис {x.ParameterType} необходимый для построения не зарегистрирован");
                    list.Add(info);
                });

            _storage.Add(
                TIn,
                new EntityInfo(TImp, TIn, lifeTime, list));
        }
    }

    public class UseValueRegisterService : IUseValueRegisterService
    {
        private readonly UseValueStorage _storage;
        private readonly CacheBase<Type, EntityInfo> _cacheBase;

        public UseValueRegisterService(UseValueStorage storage, CacheBase<Type, EntityInfo> cacheBase)
        {
            _storage = storage;
            _cacheBase = cacheBase;
        }

        public void Handle(Type TIn, Type TImp, object value, LifeTime lifeTime)
        {
            _storage.Add(TIn, value);
            //ctorParameters никогда не должны использоваться, нужно постараться этого порефакторить,
            // возможно связать со статьей по невозможности приведения generic'ов
            _cacheBase.Add(TIn, new EntityInfo(TImp, TIn, lifeTime, null));
        }
    }
}