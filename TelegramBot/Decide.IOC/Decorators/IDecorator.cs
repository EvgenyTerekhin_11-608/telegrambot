namespace DIContainer.EntityDeliverManager
{
    public interface IDecorator<TIn, TOut>
    {
        TOut Handle(TIn t);
    }
}