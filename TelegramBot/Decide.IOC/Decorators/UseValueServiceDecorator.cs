using System;

namespace DIContainer.EntityDeliverManager
{
    public class UseValueServiceDecorator : IDecorator<Type, object>
    {
        private readonly DeliverServiceCacheDecorator _deliverServiceCacheDecorator;
        private readonly UseValueStorage _useValueStorage;

        public UseValueServiceDecorator(
            DeliverServiceCacheDecorator deliverServiceCacheDecorator,
            UseValueStorage useValueStorage)
        {
            _deliverServiceCacheDecorator = deliverServiceCacheDecorator;
            _useValueStorage = useValueStorage;
        }

        public object Handle(Type t)
        {
            var value = _useValueStorage.Get(t);
            return value ?? _deliverServiceCacheDecorator.Handle(t);
        }
    }
}