using System;
using DIContainer.Cache;

namespace DIContainer.EntityDeliverManager
{
    public class DeliverServiceCacheDecorator : IDecorator<Type, object>
    {
        private readonly IDeliverService _service;
        private readonly Builder _builder;
        private readonly LifeTimeLifeTimeCacheService _lifeTimeCacheService;
        private readonly Storage _storage;

        public DeliverServiceCacheDecorator(
            IDeliverService service,
            Builder builder,
            LifeTimeLifeTimeCacheService lifeTimeCacheService)
        {
            _service = service;
            _builder = builder;
            _lifeTimeCacheService = lifeTimeCacheService;
        }

        public object Handle(Type t)
        {
            var entityInfo = _service.Get(t);
            var entity = _lifeTimeCacheService.Get(entityInfo.LifeTime, entityInfo.ImpType.FullName);
            if (entity != null)
                return entity;
            var ins = _builder.Build(entityInfo);
            _lifeTimeCacheService.Add(entityInfo.LifeTime, ins);
            return ins;
        }
    }
}