using System;

namespace DIContainer
{
    public interface IBuildLambdaRegisterService
    {
        void Handle(Type TIn,Type TImp,LifeTime lifeTime);

    }

    public interface IUseValueRegisterService
    {
        void Handle(Type TIn,Type TImp,object value,LifeTime lifeTime);
    }
}