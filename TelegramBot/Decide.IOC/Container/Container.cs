using System;
using System.Linq.Expressions;
using System.Reflection.Metadata;
using Decide.InstanceBuilder;
using DIContainer.Cache;
using DIContainer.EntityDeliverManager;

namespace DIContainer
{
    public interface IContainer
    {
        void Register<TIn, TOut>(object value, LifeTime lifeTime = LifeTime.Scoped);
        void Register<TIn, TImp>(LifeTime lifeTime = LifeTime.Scoped);
        TIn Get<TIn>();
    }

    public class Container : IContainer
    {
        private readonly RegisterManager _register;
        private readonly EntityDeliverManager.EntityDeliverManager _manager;

        public Container()
        {
            var storage = new Storage();
            var useValueStorage = new UseValueStorage();
            var registerService = new BuildLambdaRegisterService(storage,useValueStorage);
            var useValueRegisteredService = new UseValueRegisterService(useValueStorage,storage);
            var regiManger = new RegisterManager(registerService, useValueRegisteredService);
            var lambdaCacheBase = new CacheBase<string, LambdaExpression>();
            var lambdaProvider = new LambdaLifeTimeCacheService(lambdaCacheBase, new LambdaBuilder());
            var singleton = new CacheBase<string, object>();
            var scoped = new CacheBase<string, object>();
            var cacheService = new LifeTimeLifeTimeCacheService(singleton, scoped);
            var builder = new Builder(cacheService, lambdaProvider,useValueStorage);
            var deliverService = new DeliverService(storage);
            var deliverDecorator = new DeliverServiceCacheDecorator(deliverService, builder, cacheService);
            var useValueDecorator = new UseValueServiceDecorator(deliverDecorator, useValueStorage);
            var deliManager = new EntityDeliverManager.EntityDeliverManager(useValueDecorator);

            _register = regiManger;
            _manager = deliManager;
        }

        public void Register<TIn, TImp>(object value, LifeTime lifeTime = LifeTime.Scoped)
        {
            _register.Register(typeof(TIn), typeof(TImp), value, lifeTime);
        }

        public void Register(Type TIn, Type TImp, object value, LifeTime lifeTime = LifeTime.Scoped)
        {
            _register.Register(TIn, TImp, value, lifeTime);
        }

        public void Register<TIn, TImp>(LifeTime lifeTime = LifeTime.Scoped)
        {
            _register.Register(typeof(TIn), typeof(TImp), lifeTime);
        }

        public void Register(Type TInAndTOut, LifeTime lifeTime = LifeTime.Scoped)
        {
            _register.Register(TInAndTOut, TInAndTOut, lifeTime);
        }

        public void Register<TInAndTOut>(LifeTime lifeTime = LifeTime.Scoped)
        {
            _register.Register(typeof(TInAndTOut), typeof(TInAndTOut), lifeTime);
        }

        public void Register(Type TIn, Type TImp, LifeTime lifeTime = LifeTime.Scoped)
        {
            _register.Register(TIn, TImp, lifeTime);
        }

        public TIn Get<TIn>()
        {
            var result = _manager.Get<TIn>();
            return result;
        }

        public object Get(Type TIn)
        {
            var result = _manager.Get(TIn);
            return result;
        }

        public TOut Get<TIn, TOut>() where TOut : TIn
        {
            var result = (TOut) _manager.Get<TIn>();
            return result;
        }
    }
}