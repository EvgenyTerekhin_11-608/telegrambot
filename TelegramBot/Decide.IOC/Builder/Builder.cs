using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Metadata.Ecma335;
using Decide.InstanceBuilder;
using DIContainer.Cache;

namespace DIContainer
{
    public class Builder
    {
        private readonly LifeTimeLifeTimeCacheService _lifeTimeCacheService;
        private readonly ILambdaProvider _lambdaProvider;
        private readonly UseValueStorage _useValueStorage;

        public Builder(
            LifeTimeLifeTimeCacheService lifeTimeCacheService,
            ILambdaProvider lambdaProvider, UseValueStorage useValueStorage)
        {
            _lifeTimeCacheService = lifeTimeCacheService;
            _lambdaProvider = lambdaProvider;
            _useValueStorage = useValueStorage;
        }

        public object Build(EntityInfo entityInfo)
        {
            var value = _lifeTimeCacheService.Get(entityInfo.LifeTime, entityInfo.ImpType.FullName);
            if (value != null)
                return value;
            var s = _useValueStorage.GetAll();
            value = _useValueStorage.GetAll().FirstOrDefault(x => x.Key == entityInfo.InType).Value;
            if (value != null)
                return value;
            var ctors = entityInfo.CtorParameters;
            var result = Create(entityInfo,
                ctors.Count != 0
                    ? ctors.Select(Build).ToArray()
                    : new object[] { });
            return result;
        }

        public object Create(EntityInfo info, object[] arr)
        {
            var l = _lambdaProvider.Get(info);
            var compile = l.Compile();
            var r = compile.DynamicInvoke(arr);
            _lifeTimeCacheService.Add(info.LifeTime, r);
            return r;
        }
    }

    public interface ILambdaProvider
    {
        LambdaExpression Get(EntityInfo info);
    }
}