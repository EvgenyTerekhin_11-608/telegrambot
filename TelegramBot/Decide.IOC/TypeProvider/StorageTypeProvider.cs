using System;
using System.Collections.Generic;
using System.Linq;

namespace DIContainer
{
    public class StorageTypeProvider : ITypeProvider
    {
        private readonly Storage _storage;

        public StorageTypeProvider(Storage storage)
        {
            _storage = storage;
        }

        public Type GetTypeByName(string fullname, bool byFullname = true)
        {
            Type result = null;
            if (byFullname)
                result = _storage.GetAll()
                    .Select(x => x.Value.ImpType)
                    .SingleOrDefault(x => x.FullName == fullname);
            else
            {
                result = _storage.GetAll().ToList().SingleOrDefault(x => x.Key.Name == fullname).Value.ImpType;
            }

            return result;
        }

        public EntityInfo GetTypeFromStorage(Type t)
        {
            var value = _storage.Get(t);
            return value;
        }

        public IEnumerable<Type> GetTypeByName(IEnumerable<string> names, bool byFullname = true)
        {
            return names.Select(x => GetTypeByName((string) x, true));
        }
    }
}