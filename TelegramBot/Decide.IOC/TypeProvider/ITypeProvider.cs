using System;
using System.Collections.Generic;

namespace DIContainer
{
    public interface ITypeProvider
    {
        Type GetTypeByName(string fullname, bool byFullname = true);

        EntityInfo GetTypeFromStorage(Type t);
        IEnumerable<Type> GetTypeByName(IEnumerable<string> names, bool byFullname = true);
    }
}