namespace DIContainer
{
    public enum LifeTime
    {
        Singleton,
        Scoped,
        Transient
    }

}