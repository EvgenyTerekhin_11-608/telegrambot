using Microsoft.EntityFrameworkCore;
using TestFSharp;

namespace DIContainer
{
    public class IOC
    {
        public static Container Container { get; set; } = new Container();

        public IOC()
        {
            Initialize();
            AddRegistration();
        }

        private void AddRegistration()
        {
            AddDependenses();
        }

        private void Initialize()
        {
            Container = new Container();
        }


        private void AddDependenses()
        {
            Container.Register<DbContext, BotDbContext>(new BotDbContext());
        }
    }
}