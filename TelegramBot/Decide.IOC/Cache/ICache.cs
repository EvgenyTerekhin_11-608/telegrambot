using System.Collections.Generic;

namespace DIContainer.Cache
{
    public interface ICache<TIn, TOut>
    {
        void Add(TIn key, TOut value);
        TOut Get(TIn key);
        List<KeyValuePair<TIn, TOut>> GetAll();
        void Set(TIn key, TOut value);
        void Clear();
    }
}