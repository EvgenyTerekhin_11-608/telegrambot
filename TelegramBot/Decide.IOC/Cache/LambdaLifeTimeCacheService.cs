using System.Linq;
using System.Linq.Expressions;
using Decide.InstanceBuilder;
using TestFSharp;

namespace DIContainer.Cache
{
    public class LambdaLifeTimeCacheService : ILambdaProvider
    {
        private readonly CacheBase<string, LambdaExpression> _lambdaCache;
        private readonly LambdaBuilder _lambdaBuilder;

        public LambdaLifeTimeCacheService(CacheBase<string, LambdaExpression> lambdaCache, LambdaBuilder lambdaBuilder)
        {
            _lambdaCache = lambdaCache;
            _lambdaBuilder = lambdaBuilder;
        }

        public LambdaExpression Get(EntityInfo info)
        {
            var lambda = _lambdaCache.Get(info.ImpType.FullName);
            if (lambda != null)
                return lambda;
            // case if user add in constructor implementation a not interface
            var constructorParameters = info.CtorParameters.EntityToEnumerable(x => x.InType, x => x.ImpType).ToList();
            //
            var constructorCallingLambda = _lambdaBuilder.GetExpressionCtor(info.ImpType, constructorParameters);
            _lambdaCache.Add(info.ImpType.FullName, constructorCallingLambda);
            return constructorCallingLambda;
        }
    }
}