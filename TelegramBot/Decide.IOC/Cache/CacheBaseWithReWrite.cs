using System;
using System.Diagnostics;

namespace DIContainer.Cache
{
    public class CacheBaseWithReWrite<TIn, TOut> : CacheBase<TIn, TOut>
    {
        public override void Add(TIn key, TOut value)
        {
            if (_dic.TryAdd(key, value)) return;
            _dic.TryRemove(key, out _);
            if (!_dic.TryAdd(key, value))
                throw new Exception("Не удается обновить значение в словаре");
            Console.WriteLine($"Перезаписана зависимость key:{key} value:{value}");
        }
    }
}