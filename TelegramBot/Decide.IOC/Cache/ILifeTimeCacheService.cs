namespace DIContainer.Cache
{
    public interface ILifeTimeCacheService<TOut>
    {
        void Add(LifeTime time, object obj);
        TOut Get(LifeTime time, string typeFullName);
    }
}