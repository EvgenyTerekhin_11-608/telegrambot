using System.Collections.Generic;
using System.Linq;

namespace DIContainer.Cache
{
    public class LifeTimeLifeTimeCacheService : ILifeTimeCacheService<object>
    {
        private Dictionary<LifeTime, ICache<string, object>> _dic;

        public LifeTimeLifeTimeCacheService(
            ICache<string, object> singletonCache,
            ICache<string, object> scopedCache)
        {
            _dic = new Dictionary<LifeTime, ICache<string, object>>
            {
                {LifeTime.Singleton, singletonCache},
                {LifeTime.Scoped, scopedCache}
            };
        }

        public void Add(LifeTime time, object obj)
        {
            var cache = _dic.FirstOrDefault(x => x.Key == time).Value;
            cache?.Add(obj.GetType().FullName, obj);
        }

        public object Get(LifeTime time, string key)
        {
            var cache = _dic.FirstOrDefault(x => x.Key == time).Value;
            return cache?.Get(key);
        }
    }
}