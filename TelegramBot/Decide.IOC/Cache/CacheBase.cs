using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DIContainer.EntityDeliverManager;

namespace DIContainer.Cache
{
    public class CacheBase<TIn, TOut> : ICache<TIn, TOut>
    {
        protected ConcurrentDictionary<TIn, TOut> _dic { get; set; }

        public CacheBase()
        {
            _dic = new ConcurrentDictionary<TIn, TOut>();
        }

        public virtual void Add(TIn key, TOut value)
        {
            _dic.TryAdd(key, value);
        }

        public virtual List<KeyValuePair<TIn, TOut>> GetAll()
        {
            var result = _dic.ToList();
            return result;
        }

        public virtual TOut Get(TIn key)
        {
            _dic.TryGetValue(key, out var value);
            return value;
        }

        public virtual  void Set(TIn key, TOut value)
        {
            _dic.TryGetValue(key, out var val);
            _dic.TryUpdate(key, value, val);
        }

        public virtual void Clear()
        {
            _dic.Clear();
        }
    }
}