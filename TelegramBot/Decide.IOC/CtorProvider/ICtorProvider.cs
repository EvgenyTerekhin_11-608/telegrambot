using System;
using System.Collections.Generic;

namespace DIContainer
{
    public interface ICtorProvider
    {
        List<string> ParamsServices(string serviceName);
        List<Type> ParamsServices(Type serviceName);
    }
}