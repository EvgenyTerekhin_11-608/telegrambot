using System;
using System.Collections.Generic;
using System.Linq;

namespace DIContainer
{
    public class CtorProvider : ICtorProvider
    {
        private readonly Storage _storage;

        public CtorProvider(Storage storage)
        {
            _storage = storage;
        }

        public List<string> ParamsServices(string serviceName)
        {
            var result0 = _storage.GetAll()
                .Select(x => x.Value.ImpType)
                .Single(x => x.FullName == serviceName);

            var result = result0.GetConstructors()
                .OrderBy(x => x.GetParameters().Length)
                .First()
                .GetParameters()
                .Select(x => x.ParameterType.FullName)
                .ToList();

            return result;
        }

        public List<Type> ParamsServices(Type serviceName)
        {
            var result0 = _storage.GetAll()
                .Select(x => x.Value.ImpType)
                .Single(x => x.FullName == serviceName.FullName);

            var result = result0.GetConstructors()
                .OrderBy(x => x.GetParameters().Length)
                .First()
                .GetParameters()
                .Select(x=>x.ParameterType)
                .ToList();

            return result;
        }
    }
}