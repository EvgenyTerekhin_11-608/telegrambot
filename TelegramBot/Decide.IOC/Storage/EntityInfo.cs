using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DIContainer
{
    public class EntityInfo
    {
        public Type ImpType { get; set; }
        
        public Type InType { get; set; }

        public LifeTime LifeTime { get; set; }

        public List<EntityInfo> CtorParameters { get; set; }

        public EntityInfo(Type impType, Type inType, LifeTime lifeTime, List<EntityInfo> ctorParameters)
        {
            ImpType = impType;
            InType = inType;
            LifeTime = lifeTime;
            CtorParameters = ctorParameters;
        }
    }
}