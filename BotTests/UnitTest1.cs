using System;
using System.Collections.Generic;
using NUnit.Framework;
using TestFSharp;
using TestFSharp.Domain;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var educationCard = new EducationCard();
            var task1 = new InformaticTask(1, "Сколько стоит мандарие", "Дохуища");
            var task2 = new InformaticTask(1, "Сколько стоит не мандарие", "Дохуища");
            var task3 = new InformaticTask(1, "Сколько стоит укулеле", "Дохуища");

            var ans1 = new Answer(task1, "Мало");
            var ans2 = new Answer(task2, "Мало");
            var ans3 = new Answer(task3, "Мало");
            var oneHour = new TimeSpan(0, 1, 0, 0);
            var exam = new Exam(Predmet.Informatic, DateTime.Now, DateTime.Now.Add(oneHour),
                new List<InformaticTask>() {task1, task2, task3});

            exam.AddAnswer(ans1);
            exam.AddAnswer(ans2);
            exam.AddAnswer(ans3);
            var value = new AnalyzerHandler(exam).Handle();
            Assert.AreEqual(value.S_Count,0);
            Assert.AreEqual(value.F_Count,3);
        }
    }
}